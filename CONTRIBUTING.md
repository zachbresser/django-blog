# Contribute to TechHorizon:

## Documentation

---

### React

---

django-blog/TechHorizon uses React-doc-generator for generating documentation based off the code. In order for it to function properly you must have a few things:

- Top level comment above React.Component like so:

```
/**
 * This is an react component that echos a heading stored in props.
   [You must have a comment like this]
 */
class MyFirstReactComponent extends Component {
  render() {
    return <div>{this.props.heading} </div>;
  }
}
```

- PropTypes definition like so:

```
MyFirstReactComponent.propTypes = {
  /**
   * The heading to echo
   */
  heading: PropTypes.string,
};
```

Optionally default values for props can be included in the documentation like so:

```
MyFirstReactComponent.defaultProps = {
  heading: 'Tech Horizon',
};
```
