""" Test Models for Django app 'blog' """
from django.test import TestCase

from blog.tests.factories import PostFactory, CommentFactory

class PostTestCase(TestCase):
    """ Test Post """
    def setUp(self):
        """ Set up for testing """
        self.post = PostFactory()

class CommentTestCase(TestCase):
    """ Test {model} """
    def setUp(self):
        """ Set up for testing """
        self.post = PostFactory()
        self.comment = CommentFactory(parent=self.post)


