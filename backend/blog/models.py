""" Models for django app 'blog' """
import datetime
import uuid

from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.template.defaultfilters import truncatechars
from taggit.managers import TaggableManager
from django_blog.settings import MEDIA_URL

def uglify_uploaded_filename(instance, filename):
    extension = filename.split('.')[-1]
    return "{}.{}".format(uuid.uuid4(), extension)

class Profile(models.Model):
    """
    User Profiles containing Bio/Avatar/etc
    """
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    avatar = models.ImageField(upload_to=uglify_uploaded_filename, blank=True, null=True)
    bio = models.TextField(max_length=500, blank=True)
    location = models.CharField(max_length=30, blank=True)
    birth_date = models.DateField(null=True, blank=True)
    date_joined = models.DateField(auto_now_add=True)

    def __str__(self):
        return "{}'s profile".format(self.user)

    @property
    def user__username(self):
        return self.user.username

@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()

class Post(models.Model):
    """ ORM representation of Post model """
    pub_date = models.DateField(auto_now_add=True)
    author = models.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        verbose_name="post author",
        null=True,
        blank=True
    )
    title = models.CharField(max_length=50)
    content = models.TextField()
    edit_date = models.DateField(null=True, blank=True)
    slug = models.SlugField()
    tags = TaggableManager()
    thumbnail = models.ImageField(upload_to=uglify_uploaded_filename, blank=True, null=True)

    @property
    def short_content(self):
        """ Returns truncated content """
        return truncatechars(self.content, 200)

    def save(self, *args, **kwargs):
        """ Override save method to also set edit_date """
        if self.pk is None:
            super(Post, self).save(*args, **kwargs)
        else:
            self.edit_date = datetime.date.today()
            super(Post, self).save(*args, **kwargs)

    def __str__(self):
        """ String Representation of Post """
        return "{0} by: {1}".format(self.title, self.author)

    def __repr__(self):
        """ Representation of Post """
        return "class Post <\
        Pub Date: {0}, \
        Author: {1}, \
        Title: {2}, \
        Content: {3}, \
        Edit Date: {4} \
        Tags: {5}>".format(
            self.pub_date,
            self.author.username,
            self.title,
            self.content,
            self.edit_date,
            repr(self.tags)
        )

class Comment(models.Model):
    parent = models.ForeignKey(Post, on_delete=models.CASCADE)
    author = models.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        null=True,
        blank=True
    )
    content = models.TextField()

    @property
    def short_content(self):
        """ Returns truncated content """
        return truncatechars(self.content, 200)


    def __str__(self):
        """ String representation of a comment """
        return "{0}'s comment on Post: {1}".format(self.author, self.parent.title)

    def __repr__(self):
        """ Representation of this commnet """
        return "Parent: {0}, Author: {1}, Content{2}".format(
            self.parent,
            self.author,
            self.content
        )
