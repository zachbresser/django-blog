from django.contrib.auth.models import User, Group

def add_to_default_group(sender, **kwargs):
    """Add user to the BlogUsers group, creating the group
       if needed.
    """
    print("in add_to_default_group")
    user = kwargs["instance"]
    if kwargs["created"]:
        user.groups.add(Group.objects.get_or_create(name='BlogUsers')[0])