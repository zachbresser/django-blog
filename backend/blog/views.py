from django.contrib.auth.models import User
from django.shortcuts import render
from django.views.generic import TemplateView

from blog.models import Post, Comment

class Index(TemplateView):
    template_name = 'blog/index.html'
    http_method_names = ['get', 'head']
