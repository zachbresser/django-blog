from django.contrib import admin

from blog.models import Post, Comment, Profile

@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    """ Admin Model for blog.models.Post """
    list_display = ('id', 'pub_date', 'title', 'short_content', 'author')
    ordering = ['id']

@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    """ Admin model for blog.models.Comment """
    list_display = ('id', 'parent', 'author', 'short_content')
    ordering = ['id',]

@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    """ Admin Model for blog.models.Profile """
    list_display = ('user', 'avatar', 'bio', 'location', 'birth_date')
    ordering = ['user',]

    class Meta:
        readonly_fields = ('date_joined')
