""" App config for django app 'blog' """
from django.apps import AppConfig
from django.db.models.signals import post_save


class BlogConfig(AppConfig):
    """ App config for 'blog' """
    name = 'blog'
    
    def ready(self):
        """When a new user is created:
           * Add the user to the default group
        """
        from django.contrib.auth.models import User
        
        from blog.signals import add_to_default_group
        
        post_save.connect(add_to_default_group, sender=User,
                  dispatch_uid="set_default_group")
