# Generated by Django 2.1 on 2018-10-09 18:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0004_profile_favorites'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='profile',
            name='favorites',
        ),
        migrations.AddField(
            model_name='post',
            name='avatar',
            field=models.ImageField(blank=True, null=True, upload_to='post_thumbnails'),
        ),
    ]
