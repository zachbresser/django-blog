# Generated by Django 2.1 on 2018-10-09 18:42

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0005_auto_20181009_1842'),
    ]

    operations = [
        migrations.RenameField(
            model_name='post',
            old_name='avatar',
            new_name='thumbnail',
        ),
    ]
