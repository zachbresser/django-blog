"""django_blog URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path, include
from rest_framework_jwt.views import obtain_jwt_token, verify_jwt_token

from api import views

app_name = 'api'

urlpatterns = [
    path('users/', views.UserList.as_view()),
    path('users/<str:username>/', views.user_is_staff),
    path('users/<str:user__username>/profile/', views.ProfileDetailViewSet.as_view()),
    path('current_user/', views.current_user),
    path('posts/', views.PostListViewSet.as_view()),
    path('posts/<int:year>/', views.PostYearList.as_view()),
    path('posts/<int:year>/<int:month>/', views.PostYearMonthList.as_view()),
    path('posts/view/<int:pk>/', views.PostDetailViewSet.as_view()),
    path('comments/', views.CommentListViewSet.as_view()),
    path('comments/<int:pk>/', views.CommentDetailViewSet.as_view()),
    path('auth/', include('rest_auth.urls')),
    path('token-auth/', obtain_jwt_token),
    path('verify/', verify_jwt_token),
    path('email/', views.SendAdminEmail.as_view()),
]
