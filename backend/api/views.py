from algoliasearch import algoliasearch
from decouple import config
from django.contrib.auth.models import User
from django.core.mail import mail_admins
from django.shortcuts import render
from django.views.generic import TemplateView
from rest_framework import generics, permissions, status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework.views import APIView

from api.serializers import PostSerializer, CommentSerializer, \
    UserSerializer, UserSerializerWithToken, UserIsStaffSerializer, \
    ProfileSerializer, AdminEmailEndpointSerializer
from api.util import crop_photo
from blog.models import Post, Comment, Profile

@api_view(['GET'])
def current_user(request):
    """
    Determine the current user by their token, and return their data
    """
    serializer = UserSerializer(request.user)
    return Response(serializer.data)

@api_view(["GET"])
@permission_classes((permissions.AllowAny, ))
def user_is_staff(request, username):
    """
    Determine if the given username has the is_staff property
    """
    user = User.objects.get(username=username)
    serializer = UserIsStaffSerializer(user)
    return Response(serializer.data)

@api_view(["GET"])
@permission_classes((permissions.AllowAny, ))
def user_profile(request, username):
    """
    Grab profile of supplied user
    """
    profile = Profile.objects.get(user__username=username)
    serializer = ProfileSerializer(profile)
    return Response(serializer.data)


class ProfileDetailViewSet(generics.RetrieveUpdateAPIView):
    """
    API endpoint that allows User profiles to be edited or read
    """
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    lookup_field = 'user__username'
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer

    def update(self, request, *args, **kwargs):
        if request.user == kwargs['user__username'] or request.user.is_staff:
            # if authenticated user is profile user__username or the loggedIn user
            # is admin, then perform update.
            partial = kwargs.pop('partial', False)
            instance = self.get_object()
            serializer = self.get_serializer(instance, data=request.data, partial=partial)
            serializer.is_valid(raise_exception=True)
            self.perform_update(serializer)

            if getattr(instance, '_prefetched_objects_cache', None):
                # If 'prefetch_related' has been applied to a queryset, we need to
                # forcibly invalidate the prefetch cache on the instance.
                instance._prefetched_objects_cache = {}

            return Response(serializer.data)
        else:
            # not author or super user
            return Response(status=status.HTTP_401_UNAUTHORIZED, data={'details': "You are not authorized to perform this action"})



class SendAdminEmail(APIView):
    """
      View to send email to admin
    """
    permission_classes = (permissions.AllowAny, )
    def post(self, request, format=None):
        """
        Send email to admins
        """
        subject = request.data.get("subject", None)
        content = request.data.get("content", None)

        if subject and content:
            try:
                mail_admins(subject=subject, message=content)
            except BadHeaderError:
                return Response(data={ 'details': 'Invalid Header Found'}, status=status.HTTP_400_BAD_REQUEST)
            return Response(data=request.data)
        else:
            response_data = {
                'details': 'You did not supply a requested value. \
                    Ensure that your request has a from_email, subject, and email content'
            }
            return Response(data=response_data, status=status.HTTP_422_UNPROCESSABLE_ENTITY)
class PostYearList(generics.ListAPIView):
    """
    List all posts of a given year
    """
    permission_classes = (permissions.AllowAny, )
    serializer_class = PostSerializer

    def get_queryset(self):
        """
        This view should return a list of all the posts for
        the given year
        """
        year = self.kwargs['year']
        return Post.objects.filter(pub_date__icontains=year)

class PostYearMonthList(generics.ListAPIView):
    """
    List all posts of a given year and month
    """
    permission_classes = (permissions.AllowAny, )
    serializer_class = PostSerializer

    def get_queryset(self):
        """
        This view should return a list of all the posts for
        the given year and month
        """
        year, month = self.kwargs['year'], self.kwargs['month']
        print(year, month)
        return Post.objects.filter(pub_date__icontains="{year}-{month}".format(year=year, month=str(month).zfill(2)))

class UserList(APIView):
    """
    Create a new user.
    """
    permission_classes = (permissions.AllowAny,)

    def post(self, request, format=None):
        serializer = UserSerializerWithToken(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class PostListViewSet(generics.ListCreateAPIView):
    """
    API endpoint that allows Posts to be created or listed
    """
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    queryset = Post.objects.all()

    def post(self, request, format=None):
        post_item = PostSerializer(data=request.data, context={'request': request})
        if post_item.is_valid():
            if 'crop' in request.data:
                post_item.thumbnail = crop_photo(
                 request.data.crop,
                 request.data.thumbnail)
            post_item.save(author=request.user)
            # Can't use at work due to proxy blocking requests (407 Auth required)
            #
            #client = algoliasearch.Client(config("ALOGOLIA_APP_ID"), config("ALGOLIA_API_KEY"))
            #index = client.init_index(config("ALGOLIA_INDEX"))
            #res = index.add_object({
            #    "objectID": post_item.data['id'],
            #    "id": post_item.data['id'],
            #    'pub_date': post_item.data["pub_date"],
            #    'author': post_item.data['author'],
            #    'title': post_item.data["title"],
            #    'content': post_item.data["content"],
            #    'edit_date': post_item.data["edit_date"],
            #    'short_content': post_item.data["short_content"],
            #})
            return Response(post_item.data, status=status.HTTP_201_CREATED)
        else:
            return Response(post_item.errors, status=status.HTTP_400_BAD_REQUEST)
    serializer_class = PostSerializer

class PostDetailViewSet(generics.RetrieveUpdateDestroyAPIView):
    """
    API endpoint that allows Posts to be created or edited or deleted
    """
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    queryset = Post.objects.all()
    serializer_class = PostSerializer

    def destroy(self, request, *args, **kwargs):
        obj = self.get_object()
        if obj.author == request.user or request.user.is_staff:
            self.perform_destroy(obj)
            return Response(status=status.HTTP_204_NO_CONTENT)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

class CommentListViewSet(generics.ListCreateAPIView):
    """
    API endpoint that allows Comments to be viewed or edited
    """
    permission_classes = (permissions.AllowAny,)
    def get_queryset(self):
        """Overload get_queryset method and grab the postID query param
        (None if not supplied) and return a filtered queryset or all if postID
        is not supplied
        """
        postID = self.request.query_params.get('postID', None)
        if postID:
            queryset = Comment.objects.filter(parent=postID).select_related('author');
        else:
            queryset = Comment.objects.all().select_related('author')

        return queryset

    def post(self, request, format=None):
        comment = CommentSerializer(data=request.data, context={'request': request})
        if comment.is_valid():
            comment.save(author=request.user)
            return Response(comment.data, status=status.HTTP_201_CREATED)
        else:
            return Response(comment.errors, status=status.HTTP_400_BAD_REQUEST)

    serializer_class = CommentSerializer


class CommentDetailViewSet(generics.RetrieveUpdateDestroyAPIView):
    """
    API endpoint that allows Comments to created or edited or deleted
    """
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer

    def destroy(self, request, *args, **kwargs):
        obj = self.get_object()
        if obj.author == request.user or request.user.is_staff:
            self.perform_destroy(obj)
            return Response(status=status.HTTP_204_NO_CONTENT)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
