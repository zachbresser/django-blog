""" DRF Serializers for Blog Models """
from django.contrib.auth.models import User
from rest_framework import serializers
from rest_framework_jwt.settings import api_settings
from taggit_serializer.serializers import (TagListSerializerField,
                                           TaggitSerializer)

from blog.models import Post, Comment, Profile

class UserSerializer(serializers.ModelSerializer):
    """ Serializer for Builtin User Model """
    class Meta:
        model = User
        fields = (
            'is_staff',
            'first_name',
            'last_name',
            'username',
            'email',
        )

class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        exclude = ('birth_date',)

class UserIsStaffSerializer(serializers.ModelSerializer):
    """ Serializer that returns if a user is staff """
    class Meta:
        model = User
        fields = (
          'is_staff',
        )

class UserSerializerWithToken(serializers.ModelSerializer):
    """
    UserSerializer with JSONWebToken for API Auth
    """
    token = serializers.SerializerMethodField()
    password = serializers.CharField(write_only=True)

    def get_token(self, obj):
        jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
        jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

        payload = jwt_payload_handler(obj)
        token = jwt_encode_handler(payload)
        return token

    def create(self, validated_data):
        password = validated_data.pop('password', None)
        instance = self.Meta.model(**validated_data)
        if password is not None:
            instance.set_password(password)
        instance.save()
        return instance

    class Meta:
        model = User
        fields = ('token', 'username', 'password')

class PostSerializer(TaggitSerializer, serializers.ModelSerializer):
    """ Serializer for Blog Model 'Post' """
    # Below line ensures that it returns the str(User) result rather than
    # the pk for the user
    author = serializers.StringRelatedField()
    author_is_staff = serializers.SerializerMethodField()
    tags = TagListSerializerField()
    thumbnail = serializers.ImageField(use_url=True, allow_empty_file=True)

    class Meta:
        model = Post
        fields = (
            'id',
            'pub_date',
            'author',
            'title',
            'content',
            'edit_date',
            'thumbnail',
            'short_content',
            'slug',
            'author_is_staff',
            'tags'
        )
    def get_author_is_staff(self, obj):
        """Query the author attribute to determine if it is staff"""
        return obj.author.is_staff

class CommentSerializer(serializers.ModelSerializer):
    """ Serializer for Blog Model 'Comment' """
    # Below line ensures that it returns the str(User) result rather than
    # the pk for the user
    author = serializers.StringRelatedField()
    author_is_staff = serializers.SerializerMethodField()


    class Meta:
        model = Comment
        fields = (
            'id',
            'parent',
            'author',
            'author_is_staff',
            'content',
        )

    def get_author_is_staff(self, obj):
        return obj.author.is_staff

class AdminEmailEndpointSerializer(serializers.Serializer):
    """ Serializer for the Email endpoint """
    subject = serializers.CharField(max_length=300)
    content = serializers.CharField()
