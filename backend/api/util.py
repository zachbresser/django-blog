from PIL import Image

from api.serializers import UserSerializer

def my_jwt_reponse_handler(token, user=None, request=None):
    return {
        'token': token,
        'user': UserSerializer(user, context={'request': request}).data
    }

def crop_photo(crop_coord, image):
    """Use PIL to crop image, given the image,
    and the crop coordinates.

    Note: PIL expects the data in (x1, y1, x2, y2) while
    react-image-crop supplies the crop in (x, y, width, height)
    """
    img = Image.open(image)
    return img.crop(
        crop_coord.x,
        crop_coord.y,
        (crop_coord.x + crop_coord.width),
        (crop_coord.y + crop_coord.height))
