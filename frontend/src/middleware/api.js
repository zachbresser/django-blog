import axios from "axios";
import { API } from "../constants/actionTypes";
import { apiStart, apiEnd } from "../actions/apiActions";
import APIConfig from "../constants/config";

// axios default config
axios.defaults.baseURL = APIConfig.SCHEME + APIConfig.URL;
axios.defaults.headers.common["Content-Type"] = "application/json";

const api = ({ dispatch }) => next => action => {
  next(action);
  if (action.type !== API) return;
  const {
    url,
    method,
    data,
    onSuccess,
    onFailure,
    label,
    isAuthenticated
  } = action.payload;
  if (isAuthenticated) {
    axios.defaults.headers.common[
      "Authorization"
    ] = `JWT ${localStorage.getItem("token")}`;
  }
  const dataOrParams = ["GET", "DELETE"].includes(method) ? "params" : "data";

  if (label) {
    dispatch(apiStart(label));
  }
  console.log("Just before axios: ");
  axios
    .request({
      url,
      method,
      [dataOrParams]: data
    })
    .then(({ data }) => {
      dispatch(onSuccess(data));
    })
    .catch(error => {
      console.log(error);
      dispatch(onFailure(error));
    })
    .finally(() => {
      if (label) {
        dispatch(apiEnd(label));
      }
    });
};

export default api;
