import React from "react";
import { createMount } from "@material-ui/core/test-utils";
import PostListViewHeader from "../components/PostListViewHeader";
import { MemoryRouter } from "react-router";

describe("<PostListViewHeader>", () => {
  let mount;

  beforeEach(() => {
    mount = createMount();
  });

  it("renders a div", () => {
    const page = mount(
      <MemoryRouter>
        <PostListViewHeader />
      </MemoryRouter>
    );
    expect(page.find("div").length).toEqual(1);
  });

  it("renders a h2", () => {
    const page = mount(
      <MemoryRouter>
        <PostListViewHeader />
      </MemoryRouter>
    );
    expect(page.find("h2").length).toEqual(1);
  });

  it("renders an <a> tag with role=button", () => {
    const page = mount(
      <MemoryRouter>
        <PostListViewHeader />
      </MemoryRouter>
    );
    expect(page.find("a[role='button']").length).toEqual(1);
  });

  it("renders a svg", () => {
    const page = mount(
      <MemoryRouter>
        <PostListViewHeader />
      </MemoryRouter>
    );
    expect(page.find("svg").length).toEqual(1);
  });
});
