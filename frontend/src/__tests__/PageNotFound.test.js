import React from "react";
import { createMount } from "@material-ui/core/test-utils";
import PageNotFound from "../components/error/PageNotFound";

describe("<PageNotFound>", () => {
  let mount;

  beforeEach(() => {
    mount = createMount();
  });

  it("renders a div", () => {
    const page = mount(<PageNotFound />);
    expect(page.find("div").length).toEqual(1);
  });

  it("renders an image", () => {
    const page = mount(<PageNotFound />);
    expect(page.find("img").length).toEqual(1);
  });

  it("renders an h1", () => {
    const page = mount(<PageNotFound />);
    expect(page.find("h1").length).toEqual(1);
  });

  it("renders an h5", () => {
    const page = mount(<PageNotFound />);
    expect(page.find("h5").length).toEqual(1);
  });
});
