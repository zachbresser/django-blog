import React from "react";
import { createMount } from "@material-ui/core/test-utils";
import PostList from "../components/PostList";
import { MemoryRouter } from "react-router";

describe("<PostList>", () => {
  let mount;

  let posts = [
    {
      author: "zbresser",
      author_is_staff: true,
      content: "testing",
      edit_date: null,
      id: 34,
      pub_date: "2018-11-17",
      short_content: "testing",
      slug: "testing3",
      tags: ["test1", "test2"],
      thumbnail: "https://via.placeholder.com/250x140",
      title: "testing3"
    },
    {
      author: "zbresser",
      author_is_staff: true,
      content: "testing",
      edit_date: null,
      id: 33,
      pub_date: "2018-11-17",
      short_content: "testing",
      slug: "testing3",
      tags: ["test1", "test2"],
      thumbnail: "https://via.placeholder.com/250x140",
      title: "testing3"
    },
    {
      author: "zbresser",
      author_is_staff: true,
      content: "testing",
      edit_date: null,
      id: 32,
      pub_date: "2018-11-17",
      short_content: "testing",
      slug: "testing3",
      tags: ["test1", "test2"],
      thumbnail: "https://via.placeholder.com/250x140",
      title: "testing3"
    }
  ];

  beforeEach(() => {
    mount = createMount();
  });

  it("renders 3 images", () => {
    const page = mount(
      <MemoryRouter>
        <PostList posts={posts} />
      </MemoryRouter>
    );
    // one thumbnail per post
    expect(page.find("img").length).toEqual(3);
  });

  it("renders 3 h4", () => {
    const page = mount(
      <MemoryRouter>
        <PostList posts={posts} />
      </MemoryRouter>
    );
    // 1 title per post
    expect(page.find("h4").length).toEqual(3);
  });

  it("renders 9 p", () => {
    const page = mount(
      <MemoryRouter>
        <PostList posts={posts} />
      </MemoryRouter>
    );
    // 3 per post (author, date, and content)
    expect(page.find("p").length).toEqual(9);
  });

  it("renders 3 a with role=button", () => {
    const page = mount(
      <MemoryRouter>
        <PostList posts={posts} />
      </MemoryRouter>
    );
    // one see more button per post
    expect(page.find("a[role='button']").length).toEqual(3);
  });

  it("renders 16 divs", () => {
    const page = mount(
      <MemoryRouter>
        <PostList posts={posts} />
      </MemoryRouter>
    );
    // expect 16 divs, 5 per postItem (imgDiv, flexColumn, description, content,
    // and the postItem root div) as well as the containing div that holds the
    // entire list.
    expect(page.find("div").length).toEqual(16);
  });
});
