import React from "react";
import PostListView from "../containers/PostListView";
import { MemoryRouter } from "react-router";
import shallowWithStore, { mountWithStore } from "../utils/testUtils";
import { createMockStore } from "redux-test-utils";
import PostListViewHeader from "../components/PostListViewHeader";
import PostList from "../components/PostList";
import { createMount } from "@material-ui/core/test-utils";

const testState = {
  posts: [
    {
      author: "zbresser",
      author_is_staff: true,
      content: "testing",
      edit_date: null,
      id: 34,
      pub_date: "2018-11-17",
      short_content: "testing",
      slug: "testing3",
      tags: ["test1", "test2"],
      thumbnail: "https://via.placeholder.com/250x140",
      title: "testing3"
    },
    {
      author: "zbresser",
      author_is_staff: true,
      content: "testing",
      edit_date: null,
      id: 33,
      pub_date: "2018-11-17",
      short_content: "testing",
      slug: "testing3",
      tags: ["test1", "test2"],
      thumbnail: "https://via.placeholder.com/250x140",
      title: "testing3"
    },
    {
      author: "zbresser",
      author_is_staff: true,
      content: "testing",
      edit_date: null,
      id: 32,
      pub_date: "2018-11-17",
      short_content: "testing",
      slug: "testing3",
      tags: ["test1", "test2"],
      thumbnail: "https://via.placeholder.com/250x140",
      title: "testing3"
    }
  ]
};

describe("<PostListView>", () => {
  let store;
  let mount;

  beforeEach(() => {
    store = createMockStore(testState);
    mount = createMount();
  });

  it("renders without crashing", () => {
    const page = mount(
      <MemoryRouter>
        <PostListView store={store} />
      </MemoryRouter>
    );
    expect(page).toBeTruthy();
  });

  it("renders a <PostListViewHeader />", () => {
    const page = mount(
      <MemoryRouter>
        <PostListView store={store} />
      </MemoryRouter>
    );
    expect(page.find(PostListViewHeader).length).toEqual(1);
  });

  it("renders a <PostList />", () => {
    const page = mount(
      <MemoryRouter>
        <PostListView store={store} />
      </MemoryRouter>
    );
    expect(page.find(PostList).length).toEqual(1);
  });
});
