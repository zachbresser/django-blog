import { generateSlug } from "../utils/postUtils";

test("generateSlug", () => {
  const title = "My Test Title";
  expect(generateSlug(title)).toEqual("My-Test-Title");
});
