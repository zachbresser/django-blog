import React from "react";
import PropTypes from "prop-types";
import { Provider } from "react-redux";

import configureStore from "../store/configureStore";

export default class ProviderWithRouter extends React.Component {
  constructor(props) {
    super(props);

    this.store = configureStore(undefined, {
      router: props.router
    });
  }

  render() {
    return <Provider store={this.store}>{this.props.children}</Provider>;
  }
}

ProviderWithRouter.propTypes = {
  router: PropTypes.node.isRequired
};
