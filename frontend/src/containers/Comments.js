import React from "react";
import APIConfig from "../constants/config";
import CommentItem from "../components/CommentItem";
import NewComment from "../components/NewComment";
import { Badge } from "react-bootstrap";
import { connect } from "react-redux";
import _ from "lodash";
import {
  getComments,
  postComment,
  setCommentFormIsActive
} from "../actions/commentActions";
import PropTypes from "prop-types";

/**
 * Comments Component
 * @extends Component
 */
class Comments extends React.Component {
  /**
   * Constructor for Comments
   */
  constructor(props) {
    super(props);
    this.state = {
      comments: [],
      commentFormIsActive: false
    };

    this.handleNewComment = this.handleNewComment.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.deleteComment = this.deleteComment.bind(this);
  }

  deleteComment = commentID => {
    const apiURL = APIConfig.SCHEME + APIConfig.URL;
    fetch(`${apiURL + APIConfig.COMMENTS + commentID}/`, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
        Authorization: `JWT ${localStorage.getItem("token")}`
      }
    }).then(res => {
      // If deleted API returns 204 - No Content
      if (res.status === 204) {
        const comments = this.state.comments;

        const filteredComments = comments.filter(comment => {
          return comment.id !== commentID;
        });
        this.setState({ comments: filteredComments });
      } else {
        console.log(res.data);
      }
    });
  };

  handleNewComment = (e, data) => {
    this.props.postComment(data);
  };

  /**
   * After mounting component, make AJAX request to API and get comments
   * @return Promise} Not Used, sets comments var in this.state
   */
  async componentDidMount() {
    this.props.getComments(this.props.postID);
  }

  handleClick() {
    this.props.setCommentFormIsActive(!this.props.commentFormIsActive);
  }
  /**
   * Render the HTML
   * @return HTML} List of comments
   */
  render() {
    return (
      <div id="comments">
        <div className="my-1 p-2 bg-white div--rounded-corners div--shadow">
          <h4 className="font--bold">
            Comments <Badge>{this.props.comments.length}</Badge>
          </h4>
        </div>
        {this.props.comments.map((item, index) => {
          return (
            <CommentItem
              key={index}
              item={item}
              index={index}
              deleteComment={this.deleteComment}
              loggedIn={this.props.loggedIn}
              user={this.props.user}
            />
          );
        })}
        <NewComment
          postID={this.props.postID}
          numComments={this.props.comments.length}
          handleNewComment={this.handleNewComment}
          handleClick={this.handleClick}
          commentFormIsActive={this.props.commentFormIsActive}
        />
      </div>
    );
  }
}

Comments.propTypes = {
  postID: PropTypes.string.isRequired,
  deleteComment: PropTypes.func.isRequired,
  comments: PropTypes.array.isRequired,
  commentFormIsActive: PropTypes.bool.isRequired,
  selectedPost: PropTypes.number.isRequired
};

const mapStateToProps = state => ({
  comments: _.filter(
    state.comments,
    item => item.parent === state.selectedPost && item
  ),
  commentFormIsActive: state.commentFormIsActive,
  selectedPost: state.selectedPost
});

export default connect(
  mapStateToProps,
  { getComments, postComment, setCommentFormIsActive }
)(Comments);
