import React from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { loginUserAC } from "../actions/userActions";
import LoginForm from "../components/LoginForm";
import { styles } from "../styles/LoginViewStyles";
import { withStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Avatar from "@material-ui/core/Avatar";
import LockIcon from "@material-ui/icons/Lock";
import Typography from "@material-ui/core/Typography";
import Snackbar from "@material-ui/core/Snackbar";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";

/**
 * Handle the processing of Login Data and sending it to redux-saga
 * @extends React.Component
 */
class LoginView extends React.Component {
  constructor(props) {
    super(props);

    this.handleLogin = this.handleLogin.bind(this);
    this.state = {
      snackbarOpen: false
    };
  }

  /**
   * Handle login by taking user data and sending it to redux-saga to post to
   * API and set token and user state accordingly.
   * @param  {JSON} data  JSON login data
   */
  handleLogin = data => {
    const newData = {
      ...data
    };
    this.props.dispatch(loginUserAC(newData));
    setTimeout(() => {
      this.props.loggedIn && this.handleClick();
    }, 200);
  };

  handleClick = () => {
    this.setState({ snackbarOpen: true });

    setTimeout(() => {
      this.props.history.push("/");
    }, 2000);
  };

  handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    this.setState({
      snackbarOpen: false
    });
  };

  render() {
    const { classes } = this.props;
    return (
      <React.Fragment>
        <main className={classes.layout}>
          <Paper className={classes.Paper}>
            <Avatar className={classes.Avatar}>
              <LockIcon />
            </Avatar>
            <Typography variant="h5" component="h1">
              Sign in
            </Typography>
            <LoginForm handleLogin={this.handleLogin} />
          </Paper>
        </main>
        <Snackbar
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "center"
          }}
          open={this.state.snackbarOpen}
          autoHideDuration={2000}
          onClose={this.handleClose}
          ContentProps={{
            "aria-describedby": "message-id"
          }}
          message={<span id="message-id">Successfully logged in!</span>}
          action={
            <IconButton
              key="close"
              aria-label="Close"
              color="inherit"
              onClick={this.handleClose}
            >
              <CloseIcon />
            </IconButton>
          }
        />
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  loggedIn: state.loggedIn
});

LoginView.propTypes = {
  dispatch: PropTypes.func.isRequired,
  classes: PropTypes.object.isRequired,
  loggedIn: PropTypes.bool.isRequired
};
export default withStyles(styles)(
  connect(
    mapStateToProps,
    null
  )(withRouter(LoginView))
);
