import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
// import { Hits, SearchBox } from "react-instantsearch-dom";
import {
  AppBar,
  Button,
  IconButton,
  Fade,
  Toolbar,
  Typography,
  MenuItem,
  //SvgIcon,
  ListItemIcon,
  Divider,
  Avatar,
  ListItemText,
  Menu
} from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import MenuIcon from "@material-ui/icons/Menu";
import AccountCircle from "@material-ui/icons/AccountCircle";
import { Link } from "react-router-dom";
import Logo from "../assets/logo.svg";
//import PostHits from "../components/PostHits";
import { logoutUserAC } from "../actions/userActions";
import { styles } from "../styles/NavigationBarStyles";
import NavigationDrawer from "../components/NavigationDrawer";
import ProfileIcon from "../icons/ProfileIcon";
import LogoutIcon from "../icons/LogoutIcon";
import CompanyHeader from "../constants/settings";

/**
 * NavigationBar container
 * @extends React.Component
 */
class NavigationBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hitResultsShown: false,
      anchorEl: null,
      open: false
    };

    this.onChange = this.onChange.bind(this);
    this.onMouseLeave = this.onMouseLeave.bind(this);
    this.onMouseEnter = this.onMouseEnter.bind(this);
  }

  toggleDrawer = open => () => {
    this.setState({
      open
    });
  };

  handleMenu = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };
  onChange() {
    this.setState({
      hitResultsShown: true
    });
  }

  onMouseEnter(e) {
    this.setState({
      hitResultsShown: true
    });
  }

  onMouseLeave(e) {
    this.setState({
      hitResultsShown: false
    });
    let sbInput = document.getElementsByClassName("ais-SearchBox-input");
    sbInput[0].value = "";
  }

  render() {
    const AccountOpen = Boolean(this.state.anchorEl);
    const { classes } = this.props;
    const { open } = this.state;

    const logged_in_nav = (
      <React.Fragment>
        <div style={{ minWidth: "200px" }} className={classes.UserDiv}>
          {this.props.loggedIn && (
            <Typography className={`${classes.outerUserGreeting}`}>
              Hi,{" "}
              {this.props.user.first_name.charAt(0).toUpperCase() +
                this.props.user.first_name.slice(1)}
            </Typography>
          )}
          <IconButton
            aria-owns={AccountOpen ? "menu-appbar" : null}
            aria-haspopup="true"
            onClick={this.handleMenu}
            color="inherit"
          >
            <AccountCircle />
          </IconButton>
        </div>
        <Menu
          id="menu-appbar"
          anchorEl={this.state.anchorEl}
          anchorOrigin={{
            vertical: "top",
            horizontal: "right"
          }}
          open={AccountOpen}
          onClose={this.handleClose}
          TransitionComponent={Fade}
        >
          <MenuItem className={classes.hideUserGreeting}>
            <ListItemIcon>
              <Avatar className={classes.userAvatar}>
                {this.props.user.username &&
                  this.props.user.username.slice(0, 1).toUpperCase()}
              </Avatar>
            </ListItemIcon>
            <ListItemText
              className={classes.primary}
              inset
              primary={`Hello, ${this.props.user.username}`}
            />
          </MenuItem>
          <Divider className={classes.hideUserGreeting} />
          <MenuItem onClick={this.handleClose}>
            <ListItemIcon className={classes.icon}>
              <ProfileIcon />
            </ListItemIcon>
            <ListItemText inset primary="Profile" />
          </MenuItem>
          <MenuItem
            onClick={e => {
              this.handleClose();
              this.props.dispatch(logoutUserAC());
            }}
          >
            <ListItemIcon className={classes.icon}>
              <LogoutIcon />
            </ListItemIcon>
            <ListItemText inset primary="Logout" />
          </MenuItem>
        </Menu>
      </React.Fragment>
    );

    const logged_out_nav = (
      <React.Fragment>
        <Button
          component={Link}
          to="/login"
          color="inherit"
          variant="outlined"
          className={classes.appbarButton}
        >
          Login
        </Button>
        <Button
          component={Link}
          to="/signup"
          color="secondary"
          className={classes.appbarButton}
        >
          SignUp
        </Button>
      </React.Fragment>
    );

    return (
      <nav className={this.props.classes.root}>
        <AppBar>
          <Toolbar>
            <IconButton
              className={this.props.classes.menuButton}
              color="inherit"
              aria-label="Menu"
              onClick={this.toggleDrawer(true)}
            >
              <MenuIcon />
            </IconButton>
            <Link to="/" className={classes.appBarLinkNoMargin}>
              <img src={Logo} alt="logo" className={classes.appBarLogo} />
            </Link>
            <Typography
              variant="h5"
              color="inherit"
              className={
                classes.appBarTitle +
                " " +
                classes.appBarLink +
                " " +
                classes.grow
              }
            >
              <Link to="/">{CompanyHeader}</Link>
            </Typography>

            {this.props.loggedIn ? logged_in_nav : logged_out_nav}
            {/*
              <div
              className={classes.search}
              onFocus={this.onChange}
              onMouseLeave={this.onMouseLeave}
              onMouseEnter={this.onMouseEnter}
              >
              <SearchBox />
              {this.state.hitResultsShown && <Hits hitComponent={PostHits} />}
              </div>
              */}
          </Toolbar>
        </AppBar>
        <NavigationDrawer open={open} toggleDrawer={this.toggleDrawer} />
      </nav>
    );
  }
}

const mapStateToProps = state => ({
  loggedIn: state.loggedIn,
  user: state.user
});

NavigationBar.propTypes = {
  loggedIn: PropTypes.bool.isRequired,
  user: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired
};

export default connect(
  mapStateToProps,
  null
)(withStyles(styles)(NavigationBar));
