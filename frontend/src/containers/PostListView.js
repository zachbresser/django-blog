import React from "react";
import PostList from "../components/PostList";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { getPosts } from "../actions/postActions";
import PropTypes from "prop-types";
import PostListViewHeader from "../components/PostListViewHeader";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../styles/PostListViewStyles";
import _ from "lodash";

/**
 * PostListView renders a list of Posts and outputs them for
 * react-router path '/posts'
 * @extends Component
 */
class PostListView extends React.Component {
  /**
   * After mounting component, make AJAX request to API ang get posts
   * @return {Promise} Not Used, sets posts var in this.state
   */
  async componentDidMount() {
    await this.props.getPosts();
  }

  render() {
    const { classes, posts } = this.props;
    return (
      <div>
        <div className={classes.header}>
          <PostListViewHeader />
        </div>
        <PostList posts={posts} />
      </div>
    );
  }
}

PostListView.propTypes = {
  posts: PropTypes.array.isRequired,
  getPosts: PropTypes.func.isRequired,
  classes: PropTypes.object.isRequired
};

const mapStateToProps = state => {
  return {
    posts: _.orderBy(state.posts, ["id"], ["desc"])
  };
};

export default withStyles(styles)(
  connect(
    mapStateToProps,
    { getPosts }
  )(withRouter(PostListView))
);
