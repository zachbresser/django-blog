import React from "react";
import { Grid, Row, Col } from "react-bootstrap";
import PostDetail from "../components/PostDetail";
import Comments from "./Comments";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { getPosts, selectPost } from "../actions/postActions";
import { GET_POSTS } from "../constants/labels";

/**
 * Renders the HTML for an individual post
 * @extends Component
 */
class PostDetailView extends React.Component {
  async componentDidMount() {
    await this.props.getPosts();
    const postID = +this.props.match.params.postID;
    await this.props.selectPost(postID);
  }
  render() {
    const { postID } = this.props.match.params;
    return (
      <Grid fluid className="w-90 clear-nav">
        <Row className="p-md-5 bg-white div--rounded-corners div--shadow">
          <Col>
            <PostDetail selectedPost={this.props.selectedPost} />
          </Col>
        </Row>
        <Row>
          <Col>
            <Comments
              postID={postID}
              deleteComment={this.props.deleteComment}
            />
          </Col>
        </Row>
      </Grid>
    );
  }
}

const mapStateToProps = state => ({
  posts: state.posts,
  selectedPost: state.posts[state.selectedPost],
  loading: state.isLoading[GET_POSTS]
});

export default connect(
  mapStateToProps,
  { getPosts, selectPost }
)(withRouter(PostDetailView));
