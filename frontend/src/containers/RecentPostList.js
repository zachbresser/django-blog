import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../styles/RecentPostListStyles";
import { connect } from "react-redux";
import { getPosts } from "../actions/postActions";
import RecentPostListItem from "../components/RecentPostListItem";
import List from "@material-ui/core/List";
import _ from "lodash";

class RecentPostList extends React.Component {
  async componentDidMount() {
    await this.props.getPosts();
  }

  render() {
    const { classes, posts } = this.props;
    return (
      <React.Fragment>
        <List className={classes.List}>
          {posts.map &&
            posts.map(
              post => post && <RecentPostListItem key={post.id} post={post} />
            )}
        </List>
      </React.Fragment>
    );
  }
}

RecentPostList.propTypes = {
  classes: PropTypes.object.isRequired,
  posts: PropTypes.array.isRequired
};

const mapStateToProps = state => ({
  posts: _.takeRight(_.values(state.posts), 10)
});

export default connect(
  mapStateToProps,
  { getPosts }
)(withStyles(styles)(RecentPostList));
