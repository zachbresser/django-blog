import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { withRouter, Link } from "react-router-dom";
import ReactMarkdown from "react-markdown";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import KeyboardArrowRightIcon from "@material-ui/icons/KeyboardArrowRight";
import classnames from "classnames";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import Avatar from "@material-ui/core/Avatar";
import CardActions from "@material-ui/core/CardActions";
import CardMedia from "@material-ui/core/CardMedia";
import Collapse from "@material-ui/core/Collapse";
import IconButton from "@material-ui/core/IconButton";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import Divider from "@material-ui/core/Divider";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import DeleteSweepIcon from "@material-ui/icons/DeleteSweep";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import ReportIcon from "@material-ui/icons/Report";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../styles/RecentPostStyles";
import moment from "moment";
import _ from "lodash";
import { getPosts, reportPost } from "../actions/postActions";

class RecentPost extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      expanded: false,
      menuOpen: false,
      anchorEl: null
    };
  }

  async componentDidMount() {
    await this.props.getPosts();
  }

  handleExpandClick = () => {
    this.setState(state => ({ expanded: !state.expanded }));
  };

  handleMenuClick = e => {
    this.setState({ anchorEl: e.currentTarget, menuOpen: true });
  };

  handleMenuClose = e => {
    this.setState({ anchorEl: null, menuOpen: false });
  };

  render() {
    const { classes, post, user } = this.props;
    const { anchorEl, menuOpen } = this.state;

    return (
      <Paper className={classes.root} elevation={1}>
        <Typography variant="h5" className={classes.recentPost}>
          You may have missed our most recent post!
        </Typography>
        <Divider className={classes.divider} />
        <Card className={classes.card}>
          <CardHeader
            className={classes.cardHeader}
            classes={{
              title: classes.Title,
              subheader: classes.Subheader
            }}
            avatar={
              <Avatar aria-label="Post" className={classes.avatar}>
                {post && post.author && post.author.slice(0, 1).toUpperCase()}
              </Avatar>
            }
            action={
              <IconButton
                aria-owns={anchorEl ? "post-menu" : undefined}
                aria-haspopup="true"
                onClick={this.handleMenuClick}
              >
                <MoreVertIcon />
              </IconButton>
            }
            title={post && post.title && post.title}
            titleTypographyProps={{
              onClick: () => {
                this.props.history.push(`/posts/${post.id}`);
              }
            }}
            subheader={
              <React.Fragment>
                <span className={classes.subheaderAuthor}>
                  {post && post.author}
                </span>
                {post &&
                  post.author_is_staff && (
                    <span className={classes.isStaff}>[A]</span>
                  )}
                <span>
                  {/* Convert Date to Format of (September 4, 2018) */}
                  {post &&
                    post.pub_date &&
                    new Intl.DateTimeFormat("en-US", {
                      year: "numeric",
                      month: "long",
                      day: "2-digit"
                    }).format(new Date(post.pub_date))}
                </span>
              </React.Fragment>
            }
          />
          <Menu
            id="post-menu"
            anchorEl={anchorEl}
            open={menuOpen}
            onClose={this.handleMenuClose}
            transformOrigin={{ vertical: "center", horizontal: "right" }}
            className={classes.Menu}
          >
            <MenuItem
              onClick={() => {
                this.handleMenuClose();
                this.props.history.push(`/posts/${post.id}`);
              }}
            >
              <ListItemIcon>
                <ArrowForwardIcon />
              </ListItemIcon>
              <ListItemText primary="Go to Post" />
            </MenuItem>
            <Divider />
            {post &&
              (user.username === post.author || user.is_staff) && (
                <div>
                  <MenuItem
                    onClick={this.handleMenuClose}
                    className={classes.Danger}
                  >
                    <ListItemIcon>
                      <DeleteSweepIcon />
                    </ListItemIcon>
                    <ListItemText primary="Delete Post" />
                  </MenuItem>
                  <Divider />
                </div>
              )}
            <MenuItem
              className={classes.Warning}
              onClick={() => {
                this.handleMenuClose();
                this.props.reportPost(post);
              }}
            >
              <ListItemIcon>
                <ReportIcon />
              </ListItemIcon>
              <ListItemText primary="Report Post" />
            </MenuItem>
          </Menu>
          {post &&
            post.thumbnail && (
              <CardMedia
                className={classes.media}
                image={post.thumbnail}
                title="Post Thumbnail"
              />
            )}
          <CardContent>
            <Collapse
              in={!this.state.expanded}
              timeout={{ enter: 1200 }}
              unmountOnExit
            >
              {post && post.content && post.content.length > 300 ? (
                <ReactMarkdown
                  className="markdown-div"
                  source={post.content.slice(0, 300) + " ..."}
                />
              ) : (
                <ReactMarkdown
                  className="markdown-div"
                  source={post && post.content}
                />
              )}
            </Collapse>
          </CardContent>
          {post &&
            post.content &&
            post.content.length > 300 && (
              <React.Fragment>
                <CardActions>
                  <IconButton
                    className={classnames(classes.expand, {
                      [classes.expandOpen]: this.state.expanded
                    })}
                    onClick={this.handleExpandClick}
                    aria-expanded={this.state.expanded}
                    aria-label="Show more"
                  >
                    <ExpandMoreIcon />
                  </IconButton>
                </CardActions>
                <Collapse in={this.state.expanded} timeout="auto" unmountOnExit>
                  <CardContent>
                    {post.content.length > 300 && (
                      <ReactMarkdown
                        className="markdown-div"
                        source={post.content}
                      />
                    )}
                  </CardContent>
                </Collapse>
              </React.Fragment>
            )}
        </Card>
        <Divider className={classes.afterDivider} />
        <Link to="/posts/">
          <Typography className={`text-right ${classes.recentPost}`}>
            More Recent Posts
            <IconButton aria-label="More Posts">
              <KeyboardArrowRightIcon />
            </IconButton>
          </Typography>
        </Link>
      </Paper>
    );
  }
}

const mapStateToProps = state => ({
  post: _.values(state.posts).pop(), // last element in object of objects
  user: state.user
});

RecentPost.propTypes = {
  classes: PropTypes.object.isRequired,
  post: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  getPosts: PropTypes.func.isRequired,
  user: PropTypes.object.isRequired
};

RecentPost.defaultProps = {
  post: {
    id: 0,
    title: "error",
    content: "We had difficulty loading a post",
    author: "admin",
    pub_date: moment().format("YYYY-MM-DD")
  }
};
export default withRouter(
  connect(
    mapStateToProps,
    { getPosts, reportPost }
  )(withStyles(styles)(RecentPost))
);
