import React from "react";
import ReactDOM from "react-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createPost } from "../actions/postActions";
import PostCreateForm from "../components/PostCreateForm";
import PostCreatePreview from "../components/PostCreatePreview";
import PostCreateViewHeader from "../components/PostCreateViewHeader";
import PostCreateUpload from "../components/PostCreateUpload";
import { styles } from "../styles/PostCreateViewStyles";
import { withStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import moment from "moment";
import Alert from "../components/util/Alert";

/**
 * View for creating a new post with tabs to upload and crop post thumbnail,
 * as well as write and preview the post.
 * @extends React.Component
 */
class PostCreateView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tabValue: 0,
      post: {
        title: "",
        content: "",
        tagField: "",
        tags: [],
        thumbnail: null
      },
      imageData64: null,
      crop: null,
      pixelCrop: null,
      imgSrc: null
    };

    this.handleFormChange = this.handleFormChange.bind(this);
  }

  componentDidMount = () => {
    // If not loggedIn, lock tabValue and alert user
    if (!this.props.loggedIn) {
      this.setState({ tabValue: 1 });
    }
  };

  setCropState = (crop, pixelCrop) => {
    this.setState({
      crop,
      pixelCrop
    });
  };

  onPreviewDrop = (files, rejectedFiles) => {
    if (rejectedFiles && rejectedFiles.length > 0) {
      //const currentRejectFile = rejectedFiles[0];
      //const currentRejectFileSize = currentRejectFile.size;
      //if (currentRejectFileSize > this.imageMaxSize) {
      //alert("This file is too big");
      //}
    } else {
      const currentFile = files[0];
      const reader = new FileReader();
      reader.addEventListener(
        "load",
        () => {
          this.setState({
            imgSrc: reader.result
          });
        },
        false
      );

      reader.readAsDataURL(currentFile);
    }
  };

  handleThumbnailUpload = (thumbnail, imgSrc, imageData64) => {
    this.setState(prevState => {
      let post = prevState.post;
      post.thumbnail = thumbnail;
      return {
        post,
        tabValue: 1,
        imgSrc,
        imageData64
      };
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    let post = this.state.post;

    if (post.tagField !== "") {
      post.tags = [...post.tags, post.tagField].join();
    }
    post.pub_date = moment().format("YYYY-MM-DD");
    post.crop = this.state.crop;
    this.props.createPost(post);
  };
  /**
   * On form change,
   * IF name is tagField and last value is a comma,
   * THEN set tagField to "" and append to tag to tag list.
   * ELSE set name to event.target.value;
   * @param  {string} name Name of state variablle
   * @param  {event}  event React Synthetic Event
   */
  handleFormChange = name => event => {
    const value = event.target.value;
    if (name === "tagField" && value.charAt(value.length - 1) === ",") {
      setTimeout(
        this.setState(prevState => {
          const post = prevState.post;
          const tag = value.slice(0, -1);

          if (!post.tags.includes(tag)) {
            post.tags = post.tags.concat(tag);
          }
          post.tagField = "";
          return {
            post
          };
        }),
        100
      );
    } else {
      this.setState(prevState => {
        let curState = prevState;
        curState.post[name] = value;
        return curState;
      });
    }
  };

  /**
   * When chip delete button is clicked, handle the deletion of the tag,
   * and update the list accordingly.
   * @param  {event} event React Synthetic Event
   */
  handleChipDelete = event => {
    // find parent div of chip component
    const chip = ReactDOM.findDOMNode(event.target).parentNode.parentNode;
    const chipData = chip.getAttribute("data-tag");
    this.setState(prevState => {
      let prevStateTags = prevState.post.tags.filter(tag => {
        return tag !== chipData;
      });
      let curState = prevState;
      curState.post.tags = prevStateTags;
      return {
        curState
      };
    });
  };

  /**
   * Handle updating the state so that the right tab is shown
   * if not logged in, lock at tab 1
   * @param  {event} event      React Synthetic Event
   * @param  {int}   tabValue   Tab Value to switch to
   */
  handleTabChange = (event, tabValue) => {
    if (this.props.loggedIn) {
      this.setState({ tabValue });
    } else {
      return;
    }
  };

  postCanBeSubmitted = () => {
    const { title, content } = this.state.post;
    const postLongEnough = title.length > 0 && content.length > 0;
    return postLongEnough && this.props.loggedIn;
  };
  /**
   * Render the component to HTML
   * @return {JSX} JSX representation of site.
   */
  render() {
    const { classes, loggedIn } = this.props;
    const { tabValue, crop, pixelCrop, imgSrc } = this.state;

    return (
      <div className={classes.Root}>
        <PostCreateViewHeader />
        <div className={classes.PostDiv}>
          {!loggedIn && (
            <Alert variant="danger" center fullWidth>
              <span>
                You must <a href="/login">login</a> to submit a post
              </span>
            </Alert>
          )}
          <AppBar
            position="static"
            color="primary"
            className={classes.tabAppBar}
          >
            <Tabs value={tabValue} onChange={this.handleTabChange} centered>
              <Tab label="Upload Thumbnail" />
              <Tab label="Write Post" />
              <Tab label="Preview Markdown" />
            </Tabs>
          </AppBar>
          {tabValue === 0 && (
            <PostCreateUpload
              handleThumbnailUpload={this.handleThumbnailUpload}
              imgSrc={imgSrc}
              crop={crop}
              pixelCrop={pixelCrop}
              onPreviewDrop={this.onPreviewDrop}
              setCropState={this.setCropState}
            />
          )}
          {tabValue === 1 && (
            <PostCreateForm
              post={this.state.post}
              handleFormChange={this.handleFormChange}
              handleChipDelete={this.handleChipDelete}
              handleSubmit={this.handleSubmit}
              postCanBeSubmitted={this.postCanBeSubmitted}
              loggedIn={this.props.loggedIn}
            />
          )}
          {tabValue === 2 && (
            <PostCreatePreview
              content={this.state.post.content}
              title={this.state.post.title}
              tags={this.state.post.tags}
              thumbnail={this.state.imageData64}
            />
          )}
        </div>
      </div>
    );
  }
}

PostCreateView.propTypes = {
  classes: PropTypes.object.isRequired,
  loggedIn: PropTypes.bool.isRequired
};

const mapStateToProps = state => ({
  posts: state.posts,
  loggedIn: state.loggedIn
});

export default connect(
  mapStateToProps,
  { createPost }
)(withStyles(styles)(PostCreateView));
