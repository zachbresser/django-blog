import { createStore, applyMiddleware, compose } from "redux";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import rootReducer from "../reducers";
import api from "../middleware/api";
import DevTools from "../containers/DevTools";
import { createLogger } from "redux-logger";
import createSagaMiddleware from "redux-saga";
import rootSaga from "../actions/rootSaga";
import { LOGIN_USER } from "../constants/actionTypes";
import { createBrowserHistory } from "history";
import { routerMiddleware } from "connected-react-router";

export const history = createBrowserHistory();

const sagaMiddleware = createSagaMiddleware();

const rootReducerWithHistory = rootReducer(history);

// use persist key 'techHorizon' in local storage with UI State reducers not
// persisted.
const persistConfig = {
  key: "techHorizon",
  storage,
  blacklist: [
    "commentFormIsActive",
    "selectedPost",
    "isLoading",
    "router",
    "routing"
  ]
};

const persistedReducer = persistReducer(persistConfig, rootReducerWithHistory);

const configureStore = initialState => {
  const store = createStore(
    persistedReducer,
    initialState,
    compose(
      applyMiddleware(
        routerMiddleware(history),
        sagaMiddleware,
        api,
        createLogger({})
      ),
      DevTools.instrument()
    )
  );
  sagaMiddleware.run(rootSaga);
  const persiststore = persistStore(store);
  return { store, persiststore };
};

export default configureStore;
