import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../styles/PostCreateMarkdownHelpStyles";
import Typography from "@material-ui/core/Typography";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import HelpIcon from "@material-ui/icons/Help";

const PostCreateMarkdownHelp = ({ classes }) => (
  <ExpansionPanel className={classes.ExpPanel}>
    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
      <HelpIcon className={classes.HelpIcon} />
      <Typography className={classes.heading}>
        Basic Markdown Formatting
      </Typography>
    </ExpansionPanelSummary>
    <ExpansionPanelDetails>
      <div className={classes.Root}>
        <Typography variant="subtitle1">
          For <span className={classes.bold}>bold</span> text use double
          astrisks: **bold**
        </Typography>
        <Typography variant="subtitle1">
          For <span className={classes.italic}>italic</span> text use single
          astrisks: *italic*
        </Typography>
        <Typography variant="subtitle1">
          For headings prefix pound signs (up to 6, headings get smaller the
          more pound signs before it)
        </Typography>
        <Typography variant="h1"># h1</Typography>
        <Typography variant="h2">## h2</Typography>
        <Typography variant="h3">
          ### h3 <br />...etc
        </Typography>
        <Typography variant="subtitle1">
          Use three dashes to make a horizonal rule: --- <hr />
        </Typography>
        <Typography variant="subtitle1">
          Create a link like this: [I'm an inline-style
          link](https://www.google.com)<br />
          which will appear like this:{" "}
          <a href="https://www.google.com">I'm an inline-style link</a>
        </Typography>
        <br />
        <Typography variant="subtitle1">
          Create a table like this: <br />
          | Tables | Are | Cool | <br />
          | ------------- |:-------------:| -----:| <br />
          | col 3 is | right-aligned | $1600 | <br />
          | col 2 is | centered | $12 |
          <br />
          and renders to this:
          <table className={classes.exampleTable}>
            <thead>
              <tr>
                <th>Tables</th>
                <th>Are</th>
                <th>Cool</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>col 3 is</td>
                <td>right aligned</td>
                <td>$1600</td>
              </tr>
              <tr>
                <td>col 2 is</td>
                <td>centered</td>
                <td>$12</td>
              </tr>
            </tbody>
          </table>
        </Typography>
        <Typography variant="subtitle1">
          For lists, use asterisks before each line item.<br />* Item 1<br />*
          Item 2<br />
          <ul>
            <li>Item 1</li>
            <li>Item 2</li>
          </ul>
        </Typography>
        <Typography variant="subtitle1">
          For more in depth Markdown, see{" "}
          <a href="https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet">
            Adam-P's Markdown Cheatsheet
          </a>
          <br />
          Note: HTML in Markdown is not supported by TechHorizon.
        </Typography>
      </div>
    </ExpansionPanelDetails>
  </ExpansionPanel>
);

PostCreateMarkdownHelp.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(PostCreateMarkdownHelp);
