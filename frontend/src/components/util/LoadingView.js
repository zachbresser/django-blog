import React from "react";
import PropTypes from "prop-types";
import CircularProgress from "@material-ui/core/CircularProgress";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../styles/LoadingViewStyles";

const LoadingView = ({ classes }) => (
  <div className={classes.Root}>
    <CircularProgress
      className={classes.progress}
      size={100}
      color="secondary"
    />
  </div>
);

LoadingView.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(LoadingView);
