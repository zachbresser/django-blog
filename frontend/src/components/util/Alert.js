import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../styles/AlertStyles";
import CheckCircleIcon from "@material-ui/icons/CheckCircle";
import ErrorIcon from "@material-ui/icons/Error";
import InfoIcon from "@material-ui/icons/Info";
import WarningIcon from "@material-ui/icons/Warning";

const variantIcon = {
  success: CheckCircleIcon,
  warning: WarningIcon,
  danger: ErrorIcon,
  info: InfoIcon
};

const Alert = ({ classes, variant, children, center, fullWidth }) => {
  const Icon = variantIcon[variant];

  return (
    <div
      className={`${classes[variant]} ${fullWidth && classes.fullWidth}`}
      style={center && { justifyContent: "center" }}
    >
      <Icon className={classes.icon} />
      {children}
    </div>
  );
};

Alert.propTypes = {
  classes: PropTypes.object.isRequired,
  variant: PropTypes.oneOf(["danger", "warning", "info", "success"]),
  center: PropTypes.bool,
  fullWidth: PropTypes.bool
};

Alert.defaultProps = {
  center: false,
  fullWidth: false,
  variant: "info"
};

export default withStyles(styles)(Alert);
