import React from "react";
import PropTypes from "prop-types";
import { styles } from "../../styles/PageNotFoundStyles";
import { withStyles } from "@material-ui/core/styles";
import SVG404 from "../../assets/SVG/404.svg";
import Typography from "@material-ui/core/Typography";

/**
 * PageNotFound renders a 404 page for use when the url does not match any
 * declared routes.
 * @param {Object} classes Material-UI classes object
 */
const PageNotFound = ({ classes }) => (
  <div className={classes.root}>
    <img src={SVG404} alt="404" className={classes.svg404} />
    <Typography variant="h1" className={classes.h1}>
      Page Not Found
    </Typography>
    <Typography variant="h5" className={classes.h5}>
      If you believe this was in error please contact{" "}
      <a href="mailto:admin@TechHorizon.co.us">admin@techhorizon.co.us</a>
    </Typography>
  </div>
);

PageNotFound.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(PageNotFound);
