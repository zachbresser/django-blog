import React from "react";
import PropTypes from "prop-types";

/**
 * Wraps Page Content in an error boundary, showing error message
 * if there was an error. If not render children, this allows us to navigate
 * out using the navigation bar if there is an error
 * @extends React.Componet
 */
class PageContentErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError(error) {
    return { hasError: true };
  }
  render() {
    if (this.state.hasError) {
      return <h1>Something went wrong. </h1>;
    }
    return this.props.children;
  }
}

PageContentErrorBoundary.propTypes = {
  children: PropTypes.node.isRequired
};

export default PageContentErrorBoundary;
