import React from "react";
import { Grid, Row, Col, Button } from "react-bootstrap";
import NewCommentForm from "./NewCommentForm";
/**
 * React Component to handle the addition of a new comment:
 *  * POST to API
 *  * Set state to account for new comment without hitting API
 *  * render to DOM
 * @extends Component
 */
class NewComment extends React.Component {
  render() {
    return (
      <div className="my-md-2 mx-auto p-md-4 w-50 div--rounded-corners div--shadow bg-white">
        <Grid fluid>
          <Row
            className={
              this.props.commentFormIsActive ? "div--hidden" : "div--shown"
            }
          >
            <Col>
              <Button
                bsStyle="primary"
                bsSize="lg"
                className="btn--round"
                onClick={this.props.handleClick}
              >
                <i className="fas fa-plus" />
              </Button>
              <span className="font--bold">&nbsp; Add a comment</span>
            </Col>
          </Row>
          <Row
            className={
              (this.props.commentFormIsActive ? "div--shown" : "div--hidden") +
              " comment-form"
            }
          >
            <Col xs={1}>
              <Button
                bsStyle="danger"
                bsSize="small"
                className="btn--close btn--round mr-sm-2"
                onClick={this.props.handleClick}
              >
                <i className="fas fa-times" />
              </Button>
            </Col>
            <Col xs={10}>
              <NewCommentForm
                postID={this.props.postID}
                handleNewComment={this.props.handleNewComment}
              />
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default NewComment;
