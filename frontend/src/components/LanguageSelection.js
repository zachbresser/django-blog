import React from "react";
import PropTypes from "prop-types";
import Button from "@material-ui/core/Button";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../styles/LanguageSelectionStyles.js";

class LanguageSelection extends React.Component {
  handleLanguageClick = e => {
    this.props.handleLanguageClick(e.target.innerText);
  };

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.Root}>
        <Button
          color="secondary"
          variant="outlined"
          onClick={this.handleLanguageClick}
        >
          English
        </Button>
        <Button
          color="secondary"
          variant="outlined"
          onClick={this.handleLanguageClick}
        >
          Español
        </Button>
        <Button
          color="secondary"
          variant="outlined"
          onClick={this.handleLanguageClick}
        >
          Deutsche
        </Button>
        <Button
          color="secondary"
          variant="outlined"
          onClick={this.handleLanguageClick}
        >
          Italiano
        </Button>
        <Button
          color="secondary"
          variant="outlined"
          onClick={this.handleLanguageClick}
        >
          Nederlands
        </Button>
        <Button
          color="secondary"
          variant="outlined"
          onClick={this.handleLanguageClick}
        >
          Français
        </Button>
      </div>
    );
  }
}

LanguageSelection.propTypes = {
  classes: PropTypes.object.isRequired,
  handleLanguageClick: PropTypes.func.isRequired
};

export default withStyles(styles)(LanguageSelection);
