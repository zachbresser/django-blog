import React from "react";
import {
  FormGroup,
  FormControl,
  ControlLabel,
  HelpBlock,
  Button
} from "react-bootstrap";
import APIConfig from "../constants/config";

class NewCommentForm extends React.Component {
  constructor(props) {
    super(props);

    this.handleChange = this.handleChange.bind(this);
    this.handleNewComment = this.handleNewComment.bind(this);

    this.state = {
      value: "",
      user: JSON.parse(localStorage.getItem("user"))
    };
  }

  async handleNewComment(e) {
    e.preventDefault();
    const apiURL = APIConfig.SCHEME + APIConfig.URL;
    try {
      const res = await fetch(
        `${apiURL + APIConfig.USERS + this.state.user.username}/`
      );

      const results = await res.json();

      const FormContent = document.getElementsByClassName("new-comment-input");

      const data = {
        parent: this.props.postID,
        author: this.state.user.username,
        authorIsStaff: results.is_staff,
        content: FormContent[0].value
      };
      this.props.handleNewComment(e, data);
    } catch (e) {
      console.log(e);
    }
  }

  getValidationState() {
    const length = this.state.value.length;
    if (length > 20) return "success";
    else if (length > 10) return "warning";
    else if (length > 0) return "error";
    return null;
  }

  handleChange(e) {
    this.setState({
      value: e.target.value
    });
  }
  render() {
    return (
      <form onSubmit={this.handleNewComment}>
        <FormGroup
          controlId="formComment"
          validationState={this.getValidationState()}
        >
          <ControlLabel>Enter your comment below</ControlLabel>
          <FormControl
            componentClass="textarea"
            className="new-comment-input"
            value={this.state.value}
            placeholder="Enter your comment..."
            onChange={this.handleChange}
          />
          <FormControl.Feedback />
          <HelpBlock>
            To help counter spam: Comments must be at least 20 characters long
          </HelpBlock>
        </FormGroup>
        <Button type="submit" bsStyle="success">
          Submit
        </Button>
      </form>
    );
  }
}

export default NewCommentForm;
