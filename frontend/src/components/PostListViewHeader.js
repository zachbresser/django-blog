import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import AddIcon from "@material-ui/icons/Add";
import { Link } from "react-router-dom";
import { styles } from "../styles/PostListViewHeaderStyles";

const PostListViewHeader = props => {
  const { classes } = props;
  return (
    <div className={classes.HeaderDiv}>
      <Typography variant="h2" className={classes.HeaderContents}>
        Recent Posts
      </Typography>
      <Button
        component={Link}
        to="/posts/create/"
        variant="fab"
        color="secondary"
        aria-label="Create"
        className={classes.ActionButton}
      >
        <AddIcon />
      </Button>
    </div>
  );
};

PostListViewHeader.defaultProps = {
  classes: PropTypes.object.isRequired
};
export default withStyles(styles)(PostListViewHeader);
