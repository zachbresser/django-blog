import React from "react";
import { withRouter } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../styles/HomeViewStyles";
import PropTypes from "prop-types";

import RecentPost from "../containers/RecentPost";
import RecentPostList from "../containers/RecentPostList";
import HomeHeader from "../components/HomeHeader";

/**
 * Represent the HomeView for the index page
 * @extends React.Component
 */
class HomeView extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <React.Fragment>
        <HomeHeader />
        <div className={classes.Recent}>
          <RecentPost />
          <RecentPostList />
        </div>
      </React.Fragment>
    );
  }
}

HomeView.propTypes = {
  classes: PropTypes.object.isRequired
};
export default withStyles(styles)(withRouter(HomeView));
