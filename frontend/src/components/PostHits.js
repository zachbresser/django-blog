import React from "react";
import PropTypes from "prop-types";
import { Highlight } from "react-instantsearch-dom";

/**
 * Component reponsible for rendering out the autocomplete "hits"
 * @extends React.Component
 */
class PostHits extends React.Component {
  render() {
    const hit = this.props.hit;
    return (
      <div>
        <span className="hit-name">
          <a href={`/post/${hit.id}`}>
            <Highlight attribute="title" hit={hit} className="font--bold" />
          </a>
        </span>
        <p className="well">
          {hit.content.length > 150
            ? `${hit.content.slice(0, 150)}...`
            : hit.content}
        </p>
      </div>
    );
  }
}

PostHits.propTypes = {
  hit: PropTypes.object
};

PostHits.defaultProps = {
  hit: {
    id: 1,
    content: "We ran into trouble pulling results from Algolia InstantSearch"
  }
};
export default PostHits;
