import React from "react";
import {
  Grid,
  Row,
  Col,
  Media,
  Modal,
  Button,
  ButtonToolbar
} from "react-bootstrap";
import Thumbnail from "../assets/thumbnail.png";

/**
 * One individual Comment
 * @extends Component
 */
class CommentItem extends React.Component {
  constructor(props) {
    super(props);

    this.deleteComment = this.deleteComment.bind(this);
    this.editComment = this.editComment.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.showModal = this.showModal.bind(this);

    this.state = {
      showModal: false,
      commentToDelete: ""
    };
  }

  closeModal = () => {
    this.setState({ showModal: false, commentToDelete: "" });
  };

  showModal = e => {
    this.setState({ showModal: true, commentToDelete: e.target.value });
  };

  deleteComment = e => {
    e.preventDefault();
    if (this.state.commentToDelete) {
      this.props.deleteComment(parseInt(this.state.commentToDelete, 10));
      this.closeModal();
    } else {
      console.log(
        "There was no commentID in state. showModal sets the commentToDelete"
      );
    }
  };

  editComment = e => {
    const { content } = this.props.item;
    console.log(content);
  };

  render() {
      console.log("in comment item");
    const { id, author, content, author_is_staff } = this.props.item;

    const is_admin = <span className="span-isadmin">&nbsp;[A]</span>;

    return (
      <div
        key={this.props.index - 1}
        className="mt-md-2 mx-auto p-md-4 w-75 div--rounded-corners div--shadow bg-white"
      >
        <Grid fluid>
          <Row>
            <Col>
              <Media>
                <Media.Left>
                  <img
                    width={64}
                    height={64}
                    src={Thumbnail}
                    alt="thumbnail"
                    className="img--rounded"
                  />
                </Media.Left>
                <Media.Body align="bottom">
                  <Media.Heading>
                    {author}
                    {author_is_staff && is_admin}
                  </Media.Heading>
                  <hr className="m-2" />
                  <p>{content}</p>
                  <div className="position--relative">
                    <ButtonToolbar
                      className={
                        this.props.loggedIn
                          ? author === this.props.user.username ||
                            this.props.user.is_staff
                            ? "div--shown btn--delete"
                            : "div--hidden btn--delete"
                          : "div--hidden"
                      }
                    >
                      <Button
                        bsSize="small"
                        value={id}
                        className="btn--edit"
                        onClick={this.editComment}
                      >
                        Edit
                      </Button>
                      <Button
                        bsSize="small"
                        bsStyle="danger"
                        value={id}
                        onClick={this.showModal}
                      >
                        Delete
                      </Button>
                    </ButtonToolbar>
                    <Modal show={this.state.showModal} onHide={this.closeModal}>
                      <Modal.Header closeButton>
                        <Modal.Title id="contained-modal-title">
                          Delete Comment
                        </Modal.Title>
                      </Modal.Header>
                      <Modal.Body>
                        <p>Are you sure you want to delete this comment</p>
                        <p className="font--bold">
                          Note: This is an irreversible process
                        </p>
                      </Modal.Body>
                      <Modal.Footer>
                        <Button bsStyle="danger" onClick={this.deleteComment}>
                          Yes, Delete
                        </Button>
                        <Button onClick={this.closeModal}>No</Button>
                      </Modal.Footer>
                    </Modal>
                  </div>
                </Media.Body>
              </Media>
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default CommentItem;
