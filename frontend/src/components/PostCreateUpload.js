import React from "react";
import PropTypes from "prop-types";

import ReactCrop from "react-image-crop";
import PostCreateUploadDropzone from "./PostCreateUploadDropzone";
import { styles } from "../styles/PostCreateUploadStyles";
import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import {
  image64toCanvasRef,
  extractImageFileExtensionFromBase64,
  base64StringtoFile
} from "../utils/imageUtils";
import "react-image-crop/dist/ReactCrop.css";

class PostCreateUpload extends React.Component {
  constructor(props) {
    super(props);
    this.imagePreviewCanvasRef = React.createRef();
    this.state = {
      crop: {
        aspect: 16 / 9
      }
    };

    this.imageMaxSize = 4 * 1024 * 1024; // 4 Megabytes
  }

  componentDidMount = () => {
    if (this.props.crop !== null && this.props.pixelCrop !== null) {
      this.handleCropComplete(this.props.crop, this.props.pixelCrop);
    }
  };

  handleImageLoaded = image => {
    //console.log(image);
  };

  handleOnCropChange = crop => {
    this.setState({ crop });
  };

  handleCropComplete = (crop, pixelCrop) => {
    const canvasRef = this.imagePreviewCanvasRef.current;
    const { imgSrc } = this.props;

    if (canvasRef && imgSrc) {
      image64toCanvasRef(canvasRef, imgSrc, pixelCrop);
      this.props.setCropState(crop, pixelCrop);
    }
  };

  handleUploadClick = e => {
    e.preventDefault();

    const canvasRef = this.imagePreviewCanvasRef.current;
    const { imgSrc } = this.props;
    const fileExtension = extractImageFileExtensionFromBase64(imgSrc);

    const imageData64 = canvasRef.toDataURL("image/" + fileExtension);
    const fileName = "previewFile." + fileExtension;

    // file to be uploaded
    const croppedFile = base64StringtoFile(imageData64, fileName);

    this.props.handleThumbnailUpload(croppedFile, imgSrc, imageData64);
  };

  handleClearClick = e => {
    this.setState({
      crop: {
        aspect: 16 / 9
      }
    });

    // set thumbnail and imgSrc to null
    this.props.handleThumbnailUpload(null, null);
  };
  render() {
    const { imgSrc, classes } = this.props;
    const { crop } = this.state;
    return (
      <div className={classes.UploadDiv}>
        <PostCreateUploadDropzone
          onDrop={this.props.onPreviewDrop}
          maxSize={this.imageMaxSize}
        />
        {imgSrc !== null && (
          <React.Fragment>
            <div className={classes.PreviewDiv}>
              <Paper elevation={3} className={classes.Paper}>
                <Typography variant="h5">Crop</Typography>
                <ReactCrop
                  crop={crop}
                  onImageLoaded={this.handleImageLoaded}
                  onComplete={this.handleCropComplete}
                  src={imgSrc}
                  onChange={this.handleOnCropChange}
                  className={classes.Preview}
                />
              </Paper>
              <Paper elevation={2} className={classes.Paper}>
                <Typography variant="h5">Preview Crop</Typography>
                <canvas
                  ref={this.imagePreviewCanvasRef}
                  className={classes.Canvas}
                />
                <div />
              </Paper>
            </div>
            <div className={classes.buttonBar}>
              <Button
                variant="contained"
                color="secondary"
                onClick={this.handleUploadClick}
                className={classes.SelectButton}
              >
                Select Thumbnail
              </Button>
              <Button
                variant="outlined"
                color="secondary"
                className={classes.ClearButton}
                onClick={this.handleClearClick}
              >
                Clear
              </Button>
            </div>
          </React.Fragment>
        )}
      </div>
    );
  }
}

PostCreateUpload.propTypes = {
  classes: PropTypes.object.isRequired,
  handleThumbnailUpload: PropTypes.func.isRequired,
  onPreviewDrop: PropTypes.func.isRequired,
  setCropState: PropTypes.func.isRequired,
  imgSrc: PropTypes.string
};

PostCreateUpload.defaultProps = {
  imgSrc: null
};

export default withStyles(styles)(PostCreateUpload);
