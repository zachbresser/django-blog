import React from "react";
import PropTypes from "prop-types";
import { styles } from "../styles/FooterStyles";
import { withStyles } from "@material-ui/core/styles";

/**
 * Static footer of website
 * @param {object} classes material-ui style object
 */
const Footer = ({ classes }) => (
  <div className={classes.Root}>
    <div className={classes.TopFooter}>
      <div className={`${classes.Grow} ${classes.Social}`}>
        <span className={classes.h3}>Social</span>
        <div className={classes.SocialIcons}>
          <div>
            <i className="fab fa-facebook-square" />
            <span className={classes.socialFont}>&nbsp;Facebook</span>
          </div>
          <div>
            <i className="fab fa-twitter-square" />
            <span className={classes.socialFont}>&nbsp;Twitter</span>
          </div>
          <div>
            <i className="fab fa-instagram" />
            <span className={classes.socialFont}>&nbsp;Instagram</span>
          </div>
        </div>
      </div>
      <div>
        <span className={classes.h3}> Contribute</span>
        <div className={classes.Contribute}>
          <a href="https://docs.chbresser.com/project/#how-can-i-contribute-to-techhorizon">
            Contribute to Tech Horizon
          </a>
        </div>
      </div>
    </div>
    <div className={`${classes.Grow} ${classes.Center} ${classes.Copyright}`}>
      <span className={classes.CopyrightLinks}>
        Contact Us &nbsp;|&nbsp; Terms of Service &nbsp;|&nbsp; Privacy
        &nbsp;|&nbsp; Content Policy
      </span>
      <span className={classes.CopyrightHeader}>
        Copyright &copy; 2018&nbsp;|&nbsp;TechHorizon
      </span>
    </div>
  </div>
);

Footer.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Footer);
