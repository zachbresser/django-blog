import React from "react";
import PropTypes from "prop-types";
import ReactMarkdown from "react-markdown";
import { styles } from "../styles/PostListStyles";
import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import { Link } from "react-router-dom";

/**
 * Sub-Component of PostListView that renders the list of posts
 * @extends Component
 */
class PostList extends React.Component {
  render() {
    const { posts, classes } = this.props;

    return (
      <div className={classes.root}>
        {posts.map(
          post =>
            post && (
              <div className={classes.postItem} key={`${post.id}-${post.slug}`}>
                <div className={classes.imgDiv}>
                  {post.thumbnail && (
                    <img
                      src={post.thumbnail}
                      alt="Post Thumbnail"
                      key={post.thumbnail}
                      className={classes.thumbnail}
                    />
                  )}
                </div>
                <div
                  className={classes.flexColumn}
                  key={`${post.id}-flexColumns`}
                >
                  <div
                    key={`${post.id}-description`}
                    className={classes.description}
                  >
                    <Link
                      to={`/posts/${post.id}/${post.slug}/`}
                      className={classes.titleLink}
                    >
                      <Typography
                        variant="h4"
                        key={`${post.id}-${post.title}`}
                        className={classes.title}
                      >
                        {post.title}
                      </Typography>
                    </Link>
                    <span
                      key={`${post.id}-${post.author}-${post.pub_date}`}
                      className={classes.flex}
                    >
                      <Typography variant="body2" key={post.author}>
                        <span
                          key={`${post.id}-${post.author}-${
                            post.author_is_staff
                          }`}
                        >
                          {post.author}
                          {post.author_is_staff && (
                            <span
                              className={classes.isStaff}
                              key={`${post.id}-${post.author_is_staff}`}
                            >
                              [A]
                            </span>
                          )}
                        </span>
                      </Typography>
                      <Typography
                        variant="body2"
                        key={post.pub_date}
                        className={classes.pub_date}
                      >
                        {new Intl.DateTimeFormat("en-US", {
                          year: "numeric",
                          month: "long",
                          day: "2-digit"
                        }).format(new Date(post.pub_date))}
                      </Typography>
                    </span>
                    <ReactMarkdown
                      className={classes.content}
                      source={post.short_content}
                      key={post.short_content}
                    />
                  </div>

                  <Button
                    color="secondary"
                    className={classes.seeMore}
                    key={`${post.id}-seeMore`}
                    to={`/posts/${post.id}/${post.slug}/`}
                    component={Link}
                  >
                    See More
                  </Button>
                </div>
              </div>
            )
        )}
      </div>
      /*<Grid fluid className="post">
        {posts.map((post, index) => (
          <div
            className="mt-1 p-md-5 bg-white div--rounded-corners div--shadow"
            key={post.content}
          >
            <Row key={post.title + post.author}>
              <Col lg={12} key={index}>
                <span className="post__title" key={post.title}>
                  {post.title}&nbsp;
                </span>
                <span className="post__author" key={post.author}>
                  By: {post.author}&nbsp;
                </span>
                <span className="post__date" key={post.pub_date}>
                  {new Intl.DateTimeFormat("en-US", {
                    year: "numeric",
                    month: "long",
                    day: "2-digit"
                  }).format(new Date(post.pub_date))}
                </span>
              </Col>
            </Row>
            <div className="post__content rounded-corners well" key={index}>
              <ReactMarkdown
                className="markdown-div"
                source={post.short_content}
                key={post.short_content}
              />
            </div>
            <Row key={`Read more: ${post.id}`} className="mt-3">
              <Col key={post.id + index}>
                <Button bsStyle="primary" href={`/post/${post.id}`} block>
                  Read More
                </Button>
              </Col>
            </Row>
          </div>
        ))}
      </Grid>*/
    );
  }
}

PostList.propTypes = {
  posts: PropTypes.array.isRequired
};
export default withStyles(styles)(PostList);
