import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../styles/NavigationDrawerStyles";
import Drawer from "@material-ui/core/Drawer";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import Collapse from "@material-ui/core/Collapse";
import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from "@material-ui/icons/ExpandMore";
import ViewListIcon from "@material-ui/icons/ViewListOutlined";
import CreateOutlinedIcon from "@material-ui/icons/CreateOutlined";
import ShuffleIcon from "@material-ui/icons/Shuffle";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import HomeIcon from "@material-ui/icons/HomeOutlined";
import ContactsIcon from "@material-ui/icons/ContactsOutlined";
import BusinessIcon from "@material-ui/icons/BusinessOutlined";
import IconButton from "@material-ui/core/IconButton";
import DescriptionIcon from "@material-ui/icons/DescriptionOutlined";
import DashboardOutlinedIcon from "@material-ui/icons/DashboardOutlined";
import BookOutlinedIcon from "@material-ui/icons/BookOutlined";
import Divider from "@material-ui/core/Divider";
import { Link } from "react-router-dom";

class NavigationDrawer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      postsOpen: false,
      policiesOpen: false
    };
  }

  handlePostsClick = () => {
    this.setState(state => ({ postsOpen: !state.postsOpen }));
  };

  handlePoliciesClick = () => {
    this.setState(state => ({ policiesOpen: !state.policiesOpen }));
  };

  render() {
    const { classes, toggleDrawer, open } = this.props;
    const { postsOpen, policiesOpen } = this.state;

    return (
      <React.Fragment>
        <Drawer
          open={open}
          onClose={toggleDrawer(false)}
          className={classes.drawerPaper}
        >
          <div
            tabIndex={0}
            role="button"
            //onClick={toggleDrawer(false)}
            //onKeyDown={toggleDrawer(false)}
            className={classes.list}
          >
            <div className={classes.drawerHeader}>
              <IconButton
                className={classes.CloseIcon}
                onClick={toggleDrawer(false)}
              >
                <ChevronLeftIcon />
              </IconButton>
            </div>
            <Divider />
            <List>
              <ListItem
                button
                component={Link}
                to="/"
                onClick={toggleDrawer(false)}
              >
                <ListItemIcon>
                  <HomeIcon />
                </ListItemIcon>
                <ListItemText inset primary="Home" />
              </ListItem>
              <ListItem button onClick={this.handlePostsClick}>
                <ListItemIcon>
                  <DashboardOutlinedIcon />
                </ListItemIcon>
                <ListItemText inset primary="Posts" />
                {postsOpen ? <ExpandLess /> : <ExpandMore />}
              </ListItem>
              <Collapse in={postsOpen} timeout="auto" unmountOnExit>
                <List component="div" disablePadding>
                  <ListItem
                    button
                    className={classes.nested}
                    component={Link}
                    to="/posts/"
                    onClick={toggleDrawer(false)}
                  >
                    <ListItemIcon>
                      <ViewListIcon />
                    </ListItemIcon>
                    <ListItemText inset primary="Recent Posts" />
                  </ListItem>
                  <ListItem
                    button
                    className={classes.nested}
                    component={Link}
                    to="/posts/create/"
                    onClick={toggleDrawer(false)}
                  >
                    <ListItemIcon>
                      <CreateOutlinedIcon />
                    </ListItemIcon>
                    <ListItemText inset primary="Create a Post" />
                  </ListItem>
                  <ListItem button className={classes.nested}>
                    <ListItemIcon>
                      <ShuffleIcon />
                    </ListItemIcon>
                    <ListItemText inset primary="Random Post" />
                  </ListItem>
                </List>
              </Collapse>
              <ListItem
                button
                component={Link}
                to="/about"
                onClick={toggleDrawer(false)}
              >
                <ListItemIcon>
                  <BusinessIcon />
                </ListItemIcon>
                <ListItemText inset primary="About" />
              </ListItem>
              <ListItem
                button
                component={Link}
                to="/contact"
                onClick={toggleDrawer(false)}
              >
                <ListItemIcon>
                  <ContactsIcon />
                </ListItemIcon>
                <ListItemText inset primary="Contact" />
              </ListItem>
              <ListItem
                button
                component="a"
                href="http://docs.chbresser.com"
                onClick={toggleDrawer(false)}
              >
                <ListItemIcon>
                  <DescriptionIcon />
                </ListItemIcon>
                <ListItemText inset primary="Docs" />
              </ListItem>
              <Divider />
              <ListItem button onClick={this.handlePoliciesClick}>
                <ListItemIcon>
                  <BookOutlinedIcon />
                </ListItemIcon>
                <ListItemText inset primary="Important Policies" />
                {policiesOpen ? <ExpandLess /> : <ExpandMore />}
              </ListItem>
              <Collapse in={policiesOpen} timeout="auto" unmountOnExit>
                <List component="div" disablePadding>
                  <ListItem
                    button
                    className={classes.nested}
                    component={Link}
                    to="/privacy/"
                    onClick={toggleDrawer(false)}
                  >
                    <ListItemIcon>
                      <ViewListIcon />
                    </ListItemIcon>
                    <ListItemText inset primary="Privacy Policy" />
                  </ListItem>
                  <ListItem
                    button
                    className={classes.nested}
                    component={Link}
                    to="/terms/"
                    onClick={toggleDrawer(false)}
                  >
                    <ListItemIcon>
                      <ViewListIcon />
                    </ListItemIcon>
                    <ListItemText inset primary="Terms and Conditions" />
                  </ListItem>
                </List>
              </Collapse>
            </List>

            <div className={classes.drawerFooter}>
              <i
                className={`fas fa-hand-holding-heart ${classes.footerIcon}`}
              />Made with love by the TechHorizon team.
              <br />
            </div>
          </div>
        </Drawer>
      </React.Fragment>
    );
  }
}

NavigationDrawer.propTypes = {
  classes: PropTypes.object.isRequired,
  toggleDrawer: PropTypes.func.isRequired
};

export default withStyles(styles)(NavigationDrawer);
