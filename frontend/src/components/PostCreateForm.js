import React from "react";
import PropTypes from "prop-types";
import { styles } from "../styles/PostCreateFormStyles";
import { withStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Chip from "@material-ui/core/Chip";
import validator from "validator";
import PostCreateMarkdownHelp from "./PostCreateMarkdownHelp";

/**
 * Represents the Form for creating a post
 * renders the form with MUI styles, and prepares the submitted data to be sent
 * to API
 * @extends React.Component
 */
class PostCreateForm extends React.Component {
  render() {
    const { classes, postCanBeSubmitted, loggedIn } = this.props;
    const submitEnabled = postCanBeSubmitted();
    var contentLongEnough = true;
    if (this.props.post.content !== "") {
      contentLongEnough = validator.isLength(this.props.post.content, {
        min: 50
      });
    }
    var titleLongEnough = true;
    if (this.props.post.title !== "") {
      titleLongEnough = validator.isLength(this.props.post.title, {
        min: 5
      });
    }
    return (
      <form
        id="postForm"
        className={classes.container}
        noValidate
        autoComplete="off"
        onSubmit={this.props.handleSubmit}
        encType="multipart/form-data"
      >
        <TextField
          className={classes.textField}
          id="outlined-post-title"
          label="Title"
          name="title"
          value={this.props.post.title}
          onChange={this.props.handleFormChange("title")}
          variant="outlined"
          autoFocus
          fullWidth
          error={!titleLongEnough || !loggedIn}
          helperText={
            !titleLongEnough ? "Please be descriptive with your title" : ""
          }
          disabled={!loggedIn}
        />
        <TextField
          className={classes.textField}
          id="outlined-post-content"
          label="Content"
          name="content"
          value={this.props.post.content}
          onChange={this.props.handleFormChange("content")}
          variant="outlined"
          fullWidth
          multiline
          rows="20"
          error={!contentLongEnough || !loggedIn}
          helperText={
            !contentLongEnough
              ? "Please enter at least 50 characters for your post content."
              : ""
          }
          disabled={!loggedIn}
        />
        <PostCreateMarkdownHelp />
        <TextField
          className={classes.textField}
          id="outlined-post-tags"
          label="Tags"
          value={this.props.post.tagField}
          onChange={this.props.handleFormChange("tagField")}
          variant="outlined"
          helperText="Enter tags separated by commas."
          fullWidth
          disabled={!loggedIn}
          error={!loggedIn}
        />
        {this.props.post.tags.length > 0 && (
          <React.Fragment>
            <div className={classes.Tags} id="postTags">
              {this.props.post.tags.map &&
                this.props.post.tags.map((tag, index) => {
                  return (
                    <Chip
                      label={tag}
                      key={tag}
                      id={`tag-${index}`}
                      data-tag={tag}
                      color="secondary"
                      onDelete={this.props.handleChipDelete}
                      className={classes.Chip}
                    />
                  );
                })}
            </div>
          </React.Fragment>
        )}
        <div className={classes.grow}>
          <Button
            variant="contained"
            color="secondary"
            type="submit"
            disabled={!submitEnabled}
          >
            Submit
          </Button>
        </div>
        <Button variant="outlined" color="secondary">
          Cancel
        </Button>
      </form>
    );
  }
}

PostCreateForm.propTypes = {
  classes: PropTypes.object.isRequired,
  post: PropTypes.object.isRequired,
  handleFormChange: PropTypes.func.isRequired,
  handleChipDelete: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  postCanBeSubmitted: PropTypes.func.isRequired,
  loggedIn: PropTypes.bool.isRequired
};
export default withStyles(styles)(PostCreateForm);
