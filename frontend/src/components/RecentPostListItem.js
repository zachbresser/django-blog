import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import { Link } from "react-router-dom";
import { styles } from "../styles/RecentPostListItemStyles";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Avatar from "@material-ui/core/Avatar";

const RecentPostListItem = ({ classes, post }) => (
  <React.Fragment>
    <ListItem className={classes.ListItem}>
      <Avatar className={classes.avatar}>
        {post.author && post.author.slice(0, 1).toUpperCase()}
      </Avatar>
      <Link to={`/posts/${post.id}/${post.slug}/`} className={classes.PostLink}>
        <ListItemText
          primary={post.title}
          secondary={
            <React.Fragment>
              <span className={classes.author}>{post.author}</span>
              {post.author &&
                post.author_is_staff && (
                  <span className={classes.isStaff}>[A]</span>
                )}
              {post.pub_date &&
                new Intl.DateTimeFormat("en-US", {
                  year: "numeric",
                  month: "long",
                  day: "2-digit"
                }).format(new Date(post.pub_date))}
            </React.Fragment>
          }
        />
      </Link>
    </ListItem>
  </React.Fragment>
);

RecentPostListItem.propTypes = {
  classes: PropTypes.object.isRequired,
  post: PropTypes.object.isRequired
};

export default withStyles(styles)(RecentPostListItem);
