import React from "react";
import PropTypes from "prop-types";
import ReactDropzone from "react-dropzone";
import Paper from "@material-ui/core/Paper";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../styles/PostCreateUploadDropzoneStyles";

const PostCreateUploadDropzone = ({ classes, onDrop, multiple, maxSize }) => (
  <React.Fragment>
    <Paper
      elevation={3}
      style={{ width: "100%", padding: "20px", margin: "0 auto" }}
    >
      <ReactDropzone
        className={classes.DropzoneDefault}
        rejectClassName={classes.DropzoneReject}
        acceptClassName={classes.DropzoneAccept}
        accept="image/*"
        onDrop={onDrop}
        multiple={multiple}
        maxSize={maxSize}
      >
        {({ isDragAccept, isDragReject, acceptedFiles, rejectedFiles }) => {
          if (isDragAccept) {
            return "This file is authorized";
          }
          if (isDragReject) {
            return "This file is not authorized";
          }
          return "Click me to upload thumbnail. Or try dropping a file here.";
        }}
      </ReactDropzone>
    </Paper>
  </React.Fragment>
);

PostCreateUploadDropzone.propTypes = {
  classes: PropTypes.object.isRequired,
  onDrop: PropTypes.func.isRequired,
  multiple: PropTypes.bool,
  maxSize: PropTypes.number
};

PostCreateUploadDropzone.defaultProps = {
  multiple: false,
  maxSize: 4 * 1024 * 1024 // 4 MB
};
export default withStyles(styles)(PostCreateUploadDropzone);
