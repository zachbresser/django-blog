import React from "react";
import PropTypes from "prop-types";
import { styles } from "../styles/PrivacyPolicyStyles";
import { withStyles } from "@material-ui/core/styles";
import PrivacyPolicyDutch from "./privacy/PrivacyPolicyDU";
import PrivacyPolicyEnglish from "./privacy/PrivacyPolicyEN";
import PrivacyPolicyGerman from "./privacy/PrivacyPolicyGR";
import PrivacyPolicyItalian from "./privacy/PrivacyPolicyIT";
import PrivacyPolicyFrench from "./privacy/PrivacyPolicyFR";
import PrivacyPolicySpanish from "./privacy/PrivacyPolicySP";
import LanguageSelection from "./LanguageSelection";

class PrivacyPolicy extends React.Component {
  state = {
    selectedLanguage: "ENGLISH"
  };

  handleLanguageClick = language => {
    console.log(language);
    this.setState({
      selectedLanguage: language
    });
  };

  render() {
    const { classes } = this.props;
    const { selectedLanguage } = this.state;
    return (
      <div className={classes.Root}>
        <div className={classes.ButtonToolbar}>
          <LanguageSelection handleLanguageClick={this.handleLanguageClick} />
        </div>
        {selectedLanguage === "ENGLISH" && <PrivacyPolicyEnglish />}
        {selectedLanguage === "ESPAÑOL" && <PrivacyPolicySpanish />}
        {selectedLanguage === "DEUTSCHE" && <PrivacyPolicyGerman />}
        {selectedLanguage === "ITALIANO" && <PrivacyPolicyItalian />}
        {selectedLanguage === "NEDERLANDS" && <PrivacyPolicyDutch />}
        {selectedLanguage === "FRANÇAIS" && <PrivacyPolicyFrench />}
      </div>
    );
  }
}

PrivacyPolicy.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(PrivacyPolicy);
