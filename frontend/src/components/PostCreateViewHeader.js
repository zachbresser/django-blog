import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import ClearIcon from "@material-ui/icons/Clear";
import { Link } from "react-router-dom";
import { styles } from "../styles/PostCreateViewHeaderStyles";

const PostCreateViewHeader = props => {
  const { classes } = props;
  return (
    <div className={classes.HeaderDiv}>
      <Typography variant="h2" className={classes.HeaderContents}>
        Create a Post!
      </Typography>
      <Button
        component={Link}
        to="/posts/"
        variant="fab"
        color="secondary"
        aria-label="Cancel"
        className={classes.ActionButton}
      >
        <ClearIcon />
      </Button>
    </div>
  );
};

PostCreateViewHeader.defaultProps = {
  classes: PropTypes.object.isRequired
};
export default withStyles(styles)(PostCreateViewHeader);
