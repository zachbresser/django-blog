import React from "react";
import PropTypes from "prop-types";
import ReactMarkdown from "react-markdown";
import Typography from "@material-ui/core/Typography";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../styles/PostCreatePreviewStyles";
import Chip from "@material-ui/core/Chip";

/**
 * One of the tabs in PostCreateView, shows a preview of the form data from
 * PostCreateForm
 * @param {String} title - Title of Post
 * @param {String} content - Content of Post
 * @param {Array}  tags - Tags of the Post in an Array
 */
const PostCreatePreview = props => {
  const { classes } = props;
  return (
    <div className={classes.PreviewDiv}>
      {props.thumbnail && (
        <img
          className={classes.ImgPreview}
          src={props.thumbnail}
          alt="Preview Thumbnail"
        />
      )}

      <Typography variant="h5" className={classes.postTitle}>
        {props.title}
      </Typography>
      <hr />
      {props.content && (
        <ReactMarkdown source={props.content} className={classes.contentDiv} />
      )}
      <hr />
      {Array.isArray(props.tags) && (
        <div className={classes.Tags}>
          {props.tags.map(tag => {
            return (
              <Chip
                label={tag}
                key={tag}
                data-tag={tag}
                color="secondary"
                className={classes.Chip}
              />
            );
          })}
        </div>
      )}
    </div>
  );
};

PostCreatePreview.propTypes = {
  classes: PropTypes.object.isRequired,
  content: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  tags: PropTypes.array.isRequired
};

export default withStyles(styles)(PostCreatePreview);
