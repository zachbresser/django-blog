import React from "react";
import PropTypes from "prop-types";
import { Grid, Row, Col } from "react-bootstrap";
import ReactMarkdown from "react-markdown";

/**
 * Sub-Component of PostDetailView
 * @extends Component
 */
class PostDetail extends React.Component {
  /**
   * Constructor for Posts
   */
  constructor(props) {
    super(props);
    this.state = {
      post: []
    };
  }

  /**
   * Render the HTML
   * @return {HTML} A post
   */
  render() {
    return (
      <Grid fluid className="post">
        {this.props.selectedPost ? (
          <React.Fragment>
            <Row>
              <Col lg={12}>
                <span className="post__title">
                  {this.props.selectedPost.title}&nbsp;
                </span>
                <span className="post__author">
                  By: {this.props.selectedPost.author}&nbsp;
                </span>
                <span className="post__date">
                  {new Intl.DateTimeFormat("en-US", {
                    year: "numeric",
                    month: "long",
                    day: "2-digit"
                  }).format(new Date(this.props.selectedPost.pub_date))}
                </span>
                <span className="post__edit-date">
                  {this.props.selectedPost.edit_date &&
                    " Edited On " +
                      new Intl.DateTimeFormat("en-US", {
                        year: "numeric",
                        month: "long",
                        day: "2-digit"
                      }).format(new Date(this.props.selectedPost.edit_date))}
                </span>
              </Col>
            </Row>
            <div className="post__content rounded-corners well">
              <ReactMarkdown
                className="markdown-div"
                source={this.props.selectedPost.content}
              />
            </div>
          </React.Fragment>
        ) : (
          <h1>We could not find the post you are looking for!</h1>
        )}
      </Grid>
    );
  }
}

PostDetail.propTypes = {
  selectedPost: PropTypes.object.isRequired
};

export default PostDetail;
