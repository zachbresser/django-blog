import React from "react";
import PropTypes from "prop-types";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Input from "@material-ui/core/Input";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Button from "@material-ui/core/Button";
import Checkbox from "@material-ui/core/Checkbox";
export default class LoginForm extends React.Component {
  constructor(props) {
    super(props);
    // I chose to have a separate "source of truth" for controlled forms.
    // I don't want to muddy my state when it is only ever used here.
    this.state = {
      username: "",
      password: "",
      remember: false
    };
  }

  handleLogin = e => {
    e.preventDefault();
    const { username, password, remember } = this.state;
    this.props.handleLogin({ username, password, remember });
  };

  handleChange = e => {
    const name = e.target.name;
    const value = e.target.value;

    if (value === "remember") {
      this.setState(prevState => ({
        remember: !prevState.remember
      }));
    } else {
      this.setState(prevState => {
        const newState = { ...prevState };
        newState[name] = value;
        return newState;
      });
    }
  };
  render() {
    const isFilledOut =
      this.state.username.length > 0 && this.state.password.length > 0;

    return (
      <form onSubmit={this.handleLogin} onChange={this.handleChange}>
        <FormControl margin="normal" required fullWidth>
          <InputLabel htmlFor="username">Username</InputLabel>
          <Input
            id="username"
            name="username"
            autoComplete="username"
            autoFocus
            value={this.state.username}
          />
        </FormControl>
        <FormControl margin="normal" required fullWidth>
          <InputLabel htmlFor="password">Password</InputLabel>
          <Input
            name="password"
            type="password"
            id="password"
            autoComplete="current-password"
            value={this.state.password}
          />
        </FormControl>
        <FormControlLabel
          control={
            <Checkbox
              value="remember"
              color="secondary"
              checked={this.state.checked}
            />
          }
          label="Remember Me"
        />
        <Button
          type="submit"
          fullWidth
          variant="contained"
          color="secondary"
          style={{ marginTop: "1em" }}
          disabled={!isFilledOut}
        >
          Sign In
        </Button>
      </form>
    );
  }
}

LoginForm.propTypes = {
  handleLogin: PropTypes.func.isRequired
};
