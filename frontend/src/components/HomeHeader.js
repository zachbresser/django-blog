import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import { Typography } from "@material-ui/core";
import HomeCallToAction from "../assets/HomeCallToAction.jpg";
import { styles } from "../styles/CallToActionStyles";
import LazyHero from "react-lazy-hero";
import Logo from "../assets/logo.svg";

/**
 * HomeHeader
 * Represent the Header on the Home Page
 * @extends React
 */
class HomeHeader extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <React.Fragment>
        <div>
          <LazyHero
            imageSrc={HomeCallToAction}
            parallaxOffset={100}
            style={{ overflow: "hidden", position: "relative" }}
            minHeight="85vh"
            isCentered={true}
          >
            <div className={classes.LogoDiv}>
              <div className={classes.Header}>
                <img
                  src={Logo}
                  style={{ width: "500px" }}
                  alt="Tech Horizon Logo"
                />
                <Typography variant="h1" className={classes.TechHorizon}>
                  TechHorizon
                </Typography>
              </div>
            </div>
          </LazyHero>
        </div>
      </React.Fragment>
    );
  }
}

HomeHeader.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(HomeHeader);
