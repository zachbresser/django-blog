export const generateSlug = title => title.replace(/\W+/g, "-"); // replace all nonalpha chars with dash
