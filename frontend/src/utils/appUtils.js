const noOp = () => ({ type: "NO_OP" });

export const apiPayloadCreator = ({
  url = "/",
  method = "GET",
  onSuccess = noOp,
  onFailure = noOp,
  label = "",
  isAuthenticated = false,
  data = null
}) => {
  return {
    url,
    method,
    onSuccess,
    onFailure,
    isAuthenticated,
    data,
    label
  };
};
