import { createShallow } from "@material-ui/core/test-utils";
import { createMount } from "@material-ui/core/test-utils";

const shallowWithStore = (component, store) => {
  const context = {
    store
  };

  return createShallow(component, { context });
};

export const mountWithStore = (component, store) => {
  const context = {
    store
  };

  return createMount(component, { context });
};

export default shallowWithStore;
