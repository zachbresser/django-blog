import React, { Component } from "react";
import { Route, Switch, withRouter } from "react-router-dom";
import { InstantSearch } from "react-instantsearch-dom";
import NavigationBar from "./containers/NavigationBar";
import PostDetailView from "./containers/PostDetailView";
import PostListView from "./containers/PostListView";
import PostCreateView from "./containers/PostCreateView";
import HomeView from "./components/Home";
import Footer from "./components/Footer";
import "./assets/App.css";
import LoginView from "./containers/LoginView";
import { connect } from "react-redux";
import { verifyLoginAC, signupUserAC } from "./actions/userActions";
import PrivacyPolicy from "./components/PrivacyPolicy";
import TermsOfService from "./components/TermsOfService";
import CssBaseline from "@material-ui/core/CssBaseline";
import LoadingView from "./components/util/LoadingView";
import PageContentErrorBoundary from "./components/error/PageContentErrorBoundary";
import PageNotFound from "./components/error/PageNotFound";

/**
 * Main Web App
 * @extends Component
 */
class App extends Component {
  componentDidMount() {
    this.props.dispatch(verifyLoginAC());
  }

  /**
   * Handle registering a new user by retrieving token from API
   * and then adding the token and username to localStorage
   * @param  {event} e    Submit event
   * @param  {JSON} data JSON user data
   */
  handleSignup = (e, data) => {
    e.preventDefault();
    this.props.disptach(signupUserAC(data));
  };

  render() {
    return (
      <div className="App-container">
        <CssBaseline />
        <InstantSearch
          appId="0TQ4I0R7R1"
          apiKey={process.env.REACT_APP_ALGOLIA_API_KEY}
          indexName="blog_posts"
        >
          <NavigationBar />
          <PageContentErrorBoundary>
            <Switch>
              <Route exact path="/" component={HomeView} />
              <Route exact path="/posts" component={PostListView} />
              <Route exact path="/posts/create" component={PostCreateView} />
              <Route path="/posts/:postID" component={PostDetailView} />
              <Route path="/about" />
              <Route path="/contact" />
              <Route path="/login" component={LoginView} />
              <Route path="/privacy" component={PrivacyPolicy} />
              <Route path="/terms" component={TermsOfService} />
              <Route component={PageNotFound} />
            </Switch>
          </PageContentErrorBoundary>
        </InstantSearch>
        <Footer />
      </div>
    );
  }
}

export default withRouter(connect(null)(App));
