import React from "react";
import ReactDOM from "react-dom";
import { ConnectedRouter } from "connected-react-router";
import { Provider } from "react-redux";
import DevTools from "./containers/DevTools";
import { PersistGate } from "redux-persist/integration/react";
import store, { persiststore } from "./store";
import registerServiceWorker from "./registerServiceWorker";
import "./assets/index.css";
import App from "./App";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import { JssProvider } from "react-jss";
import { createGenerateClassName, jssPreset } from "@material-ui/core/styles";
import { create } from "jss";
import expand from "jss-expand";
import extend from "jss-extend";
import LoadingView from "./components/util/LoadingView";
import { history } from "./store/configureStore";
import green from "@material-ui/core/colors/green";
import amber from "@material-ui/core/colors/amber";

// Configure JSS
const jss = create({ plugins: [...jssPreset().plugins, extend(), expand()] });

// Custom Material-UI class name generator.
const generateClassName = createGenerateClassName();

const theme = createMuiTheme({
  typography: {
    useNextVariants: true
  },
  palette: {
    primary: {
      // light: will be calculated from palette.primary.main,
      main: "#3e3939",
      // dark: will be calculated from palette.primary.main,
      // contrastText: will be calculated from palette.primary.main
      bg: "#f6f4f4"
    },
    secondary: {
      main: "#ff7517"
    },
    alerts: {
      success: {
        bg: green[600]
      },
      danger: {
        bg: "#d32f2f"
      },
      warning: {
        bg: amber[700]
      },
      info: {
        bg: "#1976d2"
      },
      link: "#DEE1E0"
    }
    // error: will use the default color
  }
});
ReactDOM.render(
  <MuiThemeProvider theme={theme}>
    <Provider store={store}>
      <PersistGate persistor={persiststore} loading={<LoadingView />}>
        <JssProvider jss={jss} generateClassName={generateClassName}>
          <ConnectedRouter history={history}>
            <App />
          </ConnectedRouter>
        </JssProvider>
        <DevTools />
      </PersistGate>
    </Provider>
  </MuiThemeProvider>,
  document.getElementById("root")
);

registerServiceWorker();
