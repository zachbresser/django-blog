export const styles = theme => ({
  drawerPaper: {
    position: "relative",
    textAlign: "center"
  },
  list: {
    width: "240px",
    position: "relative",
    minHeight: "100vh"
  },
  nested: {
    paddingLeft: theme.spacing.unit * 4
  },
  CloseIcon: {
    marginLeft: "auto",
    marginRight: "0"
  },
  drawerHeader: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: "0 8px",
    ...theme.mixins.toolbar
  },

  drawerFooter: {
    borderTop: "1px dotted black",
    backgroundColor: "#f6f4f4",
    minHeight: "100px",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    position: "absolute",
    bottom: 0,
    left: 0,
    right: 0,
    padding: theme.spacing.unit * 2
  },
  footerIcon: {
    fontSize: "150%",
    color: "red"
  }
});
