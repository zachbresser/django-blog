export const styles = theme => ({
  Recent: {
    display: "flex",
    alignItems: "flex-start",
    [theme.breakpoints.down(740)]: {
      display: "block"
    }
  }
});
