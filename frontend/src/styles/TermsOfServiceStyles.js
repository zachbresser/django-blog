export const styles = theme => ({
  Root: {
    width: "80%",
    margin: "50px auto",
    backgroundColor: theme.palette.common.white,
    border: `4px solid ${theme.palette.primary.main}`,
    padding: "20px",
    webkitBoxShadow: "10px 10px 25px 0px rgba(0,0,0,0.75)",
    mozBoxShadow: "10px 10px 25px 0px rgba(0,0,0,0.75)",
    boxShadow: "10px 10px 25px 0px rgba(0,0,0,0.75)"
  }
});
