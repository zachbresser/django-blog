export const styles = theme => ({
  DropzoneDefault: {
    position: "relative",
    width: "100%",
    height: "100%",
    minHeight: "100px",
    margin: "0 auto",
    borderWidth: "2px",
    borderColor: "rgb(102, 102, 102)",
    borderStyle: "dashed",
    borderRadius: "5px",
    padding: "20px",
    textAlign: "center"
  },
  DropzoneReject: {
    borderColor: "red"
  },
  DropzoneAccept: {
    borderColor: "green"
  }
});
