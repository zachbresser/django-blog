export const styles = theme => ({
  media: {
    height: 0,
    paddingTop: "56.25%" // 16:9
  },
  actions: {
    display: "flex"
  },
  expand: {
    transform: "rotate(0deg)",
    transition: theme.transitions.create("transform", {
      duration: theme.transitions.duration.shortest
    }),
    marginLeft: "auto",
    [theme.breakpoints.up("sm")]: {
      marginRight: -8
    }
  },
  expandOpen: {
    transform: "rotate(180deg)"
  },
  avatar: {
    backgroundColor: theme.palette.secondary.main
  },
  cardHeader: {},
  card: {},
  root: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
    width: "70%",
    [theme.breakpoints.down(740)]: {
      width: "100%"
    }
  },
  recentPost: {
    fontWeight: "bold",
    padding: "1.25em 0",
    "&:after": {
      content: "",
      marginBottom: ".25em"
    },
    "&:hover": {
      textDecoration: "none"
    }
  },
  divider: {
    marginBottom: "2em"
  },
  Title: {
    fontSize: "1.25em",
    "&:hover": {
      cursor: "pointer"
    }
  },
  Subheader: {
    fontSize: ".75em"
  },
  afterDivider: {
    marginTop: "40px"
  },
  Danger: {
    color: theme.palette.alerts.danger.color,
    backgroundColor: theme.palette.alerts.danger.bg
  },
  Warning: {
    color: theme.palette.alerts.warning.color,
    backgroundColor: theme.palette.alerts.warning.bg
  },
  Menu: {
    "& ul": {
      padding: 0
    }
  },
  subheaderAuthor: {
    fontSize: "1rem",
    textTransform: "capitalize",
    fontWeight: "bold",
    marginRight: "1rem"
  },
  isStaff: {
    margin: {
      left: ".25rem",
      right: "1rem"
    },
    color: theme.palette.error.main
  }
});
