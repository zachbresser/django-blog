export const styles = theme => ({
  Preview: {
    display: "inline",
    maxWidth: "100%",
    margin: "0 auto"
  },
  PreviewDiv: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "baseline",
    marginTop: "20px"
  },
  UploadDiv: {
    padding: "20px",
    display: "flex",
    justifyContent: "center",
    flexDirection: "column",
    "& hr": {
      border: "1px solid black",
      width: "100%"
    }
  },
  Paper: {
    display: "flex",
    justifyContent: "center",
    flexDirection: "column",
    minWidth: "50%",
    margin: "20 0",
    padding: "20px",
    flexGrow: "1"
  },
  Canvas: {
    maxWidth: "100%",
    maxHeight: "100%"
  },
  SelectButton: {
    marginTop: "20px",
    width: "inherit",
    color: "white"
  },
  ClearButton: {
    marginTop: "20px",
    color: theme.palette.secondary.main
  },
  buttonBar: {
    display: "flex",
    justifyContent: "space-between"
  }
});
