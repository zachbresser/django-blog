export const styles = theme => ({
  ListItem: {
    backgroundColor: theme.palette.background.paper,
    borderBottom: "1px solid #bdbdbd",
    width: "100%",
    "&:hover": {
      textDecoration: "none"
    },
    padding: {
      top: "2em",
      bottom: "2em",
      left: "1em",
      right: "1em"
    }
  },
  avatar: {
    backgroundColor: theme.palette.secondary.main,
    margin: {
      right: "1rem"
    }
  },
  author: {
    textTransform: "capitalize",
    fontSize: "1rem",
    fontWeight: "bold",
    position: "relative"
  },
  PostLink: {
    "&:hover": {
      textDecoration: "none"
    }
  },
  isStaff: {
    margin: {
      left: ".25rem",
      right: "1rem"
    },
    color: theme.palette.error.main
  }
});
