export const styles = theme => ({
  Root: {
    width: "100%",
    padding: "10px 20px",
    minHeight: "150px",
    display: "flex",
    flexDirection: "column",
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.common.white,
    position: "absolute",
    bottom: "0",
    border: `1px solid ${theme.palette.common.black}`
  },
  Grow: {
    flexGrow: "1"
  },
  Center: {
    margin: "0 auto"
  },
  TopFooter: {
    width: "100%",
    display: "flex",
    paddingBottom: "10px",
    marginBottom: "10px",
    borderBottom: `1px solid ${theme.palette.secondary.main}`
  },
  Social: {
    display: "flex",
    flexDirection: "column",
    minWidth: "250px"
  },
  h3: {
    fontSize: "20px",
    color: theme.palette.secondary.main
  },
  socialFont: {
    marginLeft: theme.spacing.unit,
    fontSize: "14px"
  },
  Copyright: {
    fontSize: "12px",
    fontWeight: "italic",
    display: "flex",
    justifyContent: "center",
    flexDirection: "column",
    textAlign: "center"
  },
  CopyrightLinks: {
    color: theme.palette.secondary.main,
    fontSize: "14px"
  },
  CopyrightHeader: {
    borderTop: `1px solid ${theme.palette.secondary.main}`,
    marginTop: theme.spacing.unit,
    paddingTop: theme.spacing.unit
  },
  SocialIcons: {
    display: "flex",
    flexDirection: "column",
    marginLeft: "10px"
  },
  Contribute: {
    paddingLeft: "10px",
    fontSize: "14px",
    "& a": {
      color: theme.palette.common.white,

      "&:visited": {
        color: "grey"
      },
      "&:hover": {
        color: "#DADEE3"
      }
    }
  }
});
