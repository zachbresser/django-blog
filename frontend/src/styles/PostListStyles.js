export const styles = theme => ({
  root: {
    display: "flex",
    justifyContent: "center",
    flexDirection: "column",
    padding: {
      left: "1rem",
      right: "1rem"
    }
  },
  postItem: {
    display: "flex",
    backgroundColor: theme.palette.background.paper,
    margin: {
      bottom: "1rem"
    },
    border: {
      color: theme.palette.grey[50],
      size: 1,
      style: "solid"
    },
    boxShadow: theme.shadows[3]
  },
  isStaff: {
    content: "",
    width: 20,
    height: 20,
    margin: {
      left: "0.5rem"
    },
    fontSize: ".75rem",
    color: "red"
  },
  imgDiv: {
    width: "30%"
  },
  thumbnail: {
    width: "100%"
  },
  description: {
    padding: {
      right: "1rem",
      left: "1rem",
      bottom: "1rem"
    },
    maxWidth: "70%"
  },
  flex: {
    display: "flex"
  },
  pub_date: {
    margin: {
      left: "1rem"
    }
  },
  content: {
    margin: {
      top: "1rem"
    }
  },
  flexColumn: {
    display: "flex",
    flexDirection: "column",
    width: "100%"
  },
  seeMore: {
    maxWidth: 200,
    margin: {
      left: "auto",
      right: "1rem",
      bottom: "1rem"
    },
    "&:hover": {
      textDecoration: "none",
      color: theme.palette.primary.main
    }
  },
  title: {
    padding: {
      top: "1rem"
    },
    margin: {
      bottom: "1rem"
    }
  },
  titleLink: {
    "&:hover": {
      textDecoration: "none",
      "& h4": {
        color: "#666"
      }
    }
  }
});
