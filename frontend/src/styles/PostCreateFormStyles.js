export const styles = theme => ({
  container: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "center",
    padding: "20px",
    alignItems: "flex-start"
  },
  textField: {
    margin: "10px 0"
  },
  grow: {
    flexGrow: "1"
  },
  Tags: {
    width: "100%",
    padding: "5px"
  },
  Chip: {
    color: "white",
    margin: "10px 10px 10px 0",
    // this should have been default but unfortunately I had to set it
    "& svg": {
      width: "24px",
      height: "24px"
    }
  }
});
