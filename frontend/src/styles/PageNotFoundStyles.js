export const styles = theme => ({
  root: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "column",
    height: "calc(100vh - 60px - 150px)"
  },
  svg404: {
    width: "100%",
    maxWidth: "650px"
  },
  h1: {
    color: "#555 !important"
  },
  h5: {
    color: "#555 !important",
    marginTop: "1rem"
  }
});
