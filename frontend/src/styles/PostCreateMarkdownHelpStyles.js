export const styles = theme => ({
  Root: {
    backgroundColor: "#E3C95C",
    border: "1px dashed #574D23",
    color: "#565746",
    width: "100%",
    padding: "10px",
    display: "flex",
    flexDirection: "column",
    marginBottom: "10px"
  },
  bold: {
    fontWeight: "bold"
  },
  italic: {
    fontStyle: "italic"
  },
  ExpPanel: {
    width: "100%"
  },
  HelpIcon: {
    marginRight: "5px"
  },
  exampleTable: {
    "& td, & th": {
      padding: "10px",
      border: "1px solid black"
    },
    "& th": {
      textAlign: "center"
    },
    "& td:nth-child(2)": {
      textAlign: "center"
    },
    "& td:nth-child(3)": {
      textAlign: "right"
    },
    border: "1px solid black"
  }
});
