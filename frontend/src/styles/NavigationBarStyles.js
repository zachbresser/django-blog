import { fade } from "@material-ui/core/styles/colorManipulator";
import red from "@material-ui/core/colors/red";

export const styles = theme => ({
  root: {
    flexGrow: 1,
    "& .transparent": {
      background: "transparent"
    }
  },
  grow: {
    flexGrow: 1
  },
  appBarLink: {
    color: theme.palette.common.white,
    marginRight: theme.spacing.unit * 5,
    "& a": {
      color: theme.palette.common.white,
      textDecoration: "none"
    },
    "& a:hover": {
      color: theme.palette.primary.bg
    }
  },
  userAvatar: {
    width: "24px",
    height: "24px",
    backgroundColor: red[500],
    color: theme.palette.common.white
  },
  appBarLogo: {
    width: "100px",
    margin: {
      right: 10
    }
  },
  appBarLinkNoMargin: {
    color: theme.palette.common.white,
    "& a": {
      color: theme.palette.common.white,
      textDecoration: "none"
    }
  },
  appBarButton: {
    color: theme.palette.common.white,
    "& a": {
      color: theme.palette.common.white,
      textDecoration: "none"
    }
  },
  appBarTitle: {
    marginRight: theme.spacing.unit * 5,
    paddingRight: theme.spacing.unit * 2,
    color: theme.palette.common.white,
    borderRight: `${theme.spacing.unit / 8}px solid ${
      theme.palette.common.white
    } !important`,
    [theme.breakpoints.down("sm")]: {
      display: "none"
    }
  },
  icon: {
    marginRight: theme.spacing.unit * 2
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20
  },
  title: {
    width: theme.spacing.unit * 250
  },
  menuItem: {
    "&:focus": {
      backgroundColor: theme.palette.primary.main,
      "& $primary, & $icon": {
        color: theme.palette.common.white
      }
    }
  },
  primary: {
    //fontSize: "40% !important",
    //"& span": {
    //  fontSize: "40% !important"
    //  }
  },
  search: {
    position: "relative",
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    "&:hover": {
      backgroundColor: fade(theme.palette.common.white, 0.25)
    },
    marginLeft: 0,
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      marginLeft: theme.spacing.unit,
      width: "auto"
    },
    [theme.breakpoints.down("sm")]: {
      display: "none"
    }
  },
  appbarButton: {
    fontSize: "100%",
    "&:hover": {
      color: theme.palette.common.white
    }
  },
  avatar: {
    backgroundColor: red[500]
  },
  inputRoot: {
    color: "inherit",
    width: "100%"
  },
  inputInput: {
    paddingTop: theme.spacing.unit,
    paddingRight: theme.spacing.unit,
    paddingBottom: theme.spacing.unit,
    paddingLeft: theme.spacing.unit * 10,
    transition: theme.transitions.create("width"),
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      width: 120,
      "&:focus": {
        width: 200
      }
    }
  },
  hideUserGreeting: {
    [theme.breakpoints.up(720)]: {
      display: "none"
    }
  },
  outerUserGreeting: {
    display: "none",
    [theme.breakpoints.up(720)]: {
      display: "inline-block",
      color: theme.palette.common.white
    }
  },
  UserDiv: {
    minWidth: 200
  }
});
