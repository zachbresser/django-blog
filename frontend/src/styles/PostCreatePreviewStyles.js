export const styles = theme => ({
  postTitle: {
    margin: "20px 0",
    textAlign: "center",
    fontWeight: "bold"
  },
  PreviewDiv: {
    padding: "20px"
  },
  contentDiv: {
    padding: "20px",
    border: "1px solid rgba(0, 0, 0, .3)",
    borderRadius: "3px",
    "& hr": {
      borderColor: "black"
    }
  },
  Tags: {
    padding: "10px"
  },
  bold: {
    fontWeight: "bold"
  },
  Chip: {
    color: "white",
    margin: "30px 10px 30px 0"
  },
  ImgPreview: {
    margin: "0 auto",
    display: "block"
  }
});
