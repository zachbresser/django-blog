export const styles = theme => ({
  divContainer: {
    position: "relative",
    textAlign: "center",
    color: "white",
    width: "100%",
    "& button": {
      fontSize: "110%"
    },
    "& h1": {
      fontSize: "48px"
    }
  },
  button: {
    margin: theme.spacing.unit,
    "&:hover": {
      transform: "scale(1.1) translateY(-2px)",
      color: theme.palette.secondary.main
    }
  },
  SignUpButton: {
    margin: theme.spacing.unit,
    "&:hover": {
      transform: "scale(1.1) translateY(-2px)",
      webkitBoxShadow: "0px 3px 8px 0px rgba(0,0,0,0.75)",
      mozBoxShadow: "0px 3px 8px 0px rgba(0,0,0,0.75)",
      boxShadow: "0px 3px 8px 0px rgba(0,0,0,0.75)",
      color: theme.palette.common.white
    }
  },
  LogoDiv: {
    width: "100%",
    "& img": {
      [theme.breakpoints.down(740)]: {
        display: "none"
      }
    }
  },
  TechHorizon: {
    fontWeight: "bold",
    [theme.breakpoints.down(740)]: {
      fontSize: "3rem"
    },
    color: theme.palette.secondary.main,
    background:
      "linear-gradient(45deg, rgba(178,81,16,1) 0%, rgba(178,81,16,1) 29%, rgba(62,57,57,1) 52%, rgba(178,81,16,1) 76%, rgba(178,81,16,1) 100%)",
    textTransform: "uppercase",
    "-webkit-background-clip": "text",
    "-webkit-text-fill-color": "transparent"
  }
});
