export const styles = theme => ({
  Root: {
    padding: "20px"
  },
  PostDiv: {
    margin: "30px auto",
    backgroundColor: theme.palette.background.paper,
    webkitBoxShadow: "5px 5px 10px 0px rgba(0,0,0,0.33)",
    mozBoxShadow: "5px 5px 10px 0px rgba(0,0,0,0.33)",
    boxShadow: "5px 5px 10px 0px rgba(0,0,0,0.33)"
  },
  tabAppBar: {}
});
