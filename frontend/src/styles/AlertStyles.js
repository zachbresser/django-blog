export const styles = theme => ({
  // Apply Global stylings to Root, then use extend (using jss-extend)
  // Then apply special stylings to each individual
  Root: {
    padding: "1em",
    margin: "0 auto",
    boxShadow: {
      x: 2,
      y: 2,
      blur: 2,
      opacity: ".8"
    },
    "max-width": "50%",
    color: theme.palette.common.white,
    display: "flex",
    alignItems: "center"
  },
  danger: {
    extend: "Root",
    backgroundColor: theme.palette.alerts.danger.bg,
    // below repeats for every variant, it was necessary because
    // jss-nested "&" was not working properly with jss-extend "extend: 'Root'"
    "& a": {
      textDecoration: "none",
      fontWeight: "bold",
      color: theme.palette.alerts.link,
      "&:hover": {
        textDecoration: "none",
        color: theme.palette.grey[50]
      }
    }
  },
  warning: {
    extend: "Root",
    backgroundColor: theme.palette.alerts.warning.bg,
    "& a": {
      textDecoration: "none",
      fontWeight: "bold",
      color: theme.palette.alerts.link,
      "&:hover": {
        textDecoration: "none",
        color: theme.palette.grey[50]
      }
    }
  },
  info: {
    extend: "Root",
    backgroundColor: theme.palette.alerts.info.bg,
    "& a": {
      textDecoration: "none",
      fontWeight: "bold",
      color: theme.palette.alerts.link,
      "&:hover": {
        textDecoration: "none",
        color: theme.palette.grey[50]
      }
    }
  },
  success: {
    extend: "Root",
    backgroundColor: theme.palette.alerts.success.bg,
    "& a": {
      textDecoration: "none",
      fontWeight: "bold",
      color: theme.palette.alerts.link,
      "&:hover": {
        textDecoration: "none",
        color: theme.palette.grey[50]
      }
    }
  },
  center: {
    textAlign: "center"
  },
  fullWidth: {
    minWidth: "100%"
  },
  icon: {
    fontSize: 20,
    opacity: 0.9,
    marginRight: theme.spacing.unit
  }
});
