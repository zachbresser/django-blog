export const styles = theme => ({
  List: {
    padding: 0,
    margin: {
      left: 1
    },
    backgroundColor: theme.palette.background.paper,
    borderRadius: 4,
    boxShadow: theme.shadows[1],
    minWidth: "30%",
    [theme.breakpoints.down(740)]: {
      minWidth: "100%",
      display: "inline-block"
    }
  }
});
