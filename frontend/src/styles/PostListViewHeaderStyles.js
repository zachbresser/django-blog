export const styles = theme => ({
  HeaderDiv: {
    paddingBottom: "20px",
    borderBottom: "1px dotted #000",
    display: "flex"
  },
  ActionButton: {
    color: theme.palette.common.white
  },
  HeaderContents: {
    flexGrow: 1
  }
});
