import { createAction } from "redux-actions";
import { normalize, schema } from "normalizr";
import { GET_COMMENTS } from "../constants/labels";
import {
  API,
  SET_COMMENTS,
  ADD_COMMENT,
  SET_COMMENT_FORM_IS_ACTIVE
} from "../constants/actionTypes";
import { apiPayloadCreator } from "../utils/appUtils";

const getCommentsAC = createAction(API, apiPayloadCreator);

export const getComments = postID => {
  return getCommentsAC({
    url: `/comments/`,
    method: "GET",
    onSuccess: setComments,
    label: GET_COMMENTS
  });
};

const postCommentsAC = createAction(API, apiPayloadCreator);

export const postComment = data => {
  return postCommentsAC({
    url: "/comments/",
    method: "POST",
    isAuthenticated: true,
    onSuccess: addComment,
    data: data
  });
};

function addComment(comment) {
  console.log("in add comment");
  const normalizedData = { [comment.id]: comment };
  setCommentFormIsActive(false);
  return {
    type: ADD_COMMENT,
    payload: normalizedData
  };
}

function setComments(comments) {
  const commentSchema = new schema.Entity("comments");
  const commentListSchema = new schema.Array(commentSchema);
  const normalizedData = normalize(comments, commentListSchema);

  return {
    type: SET_COMMENTS,
    payload: normalizedData.entities.comments
  };
}

export const setCommentFormIsActive = createAction(SET_COMMENT_FORM_IS_ACTIVE);
