import { createAction } from "redux-actions";
import { normalize, schema } from "normalizr";
import { GET_POSTS } from "../constants/labels";
import {
  API,
  SET_POSTS,
  SELECT_POST,
  REPORT_POST,
  CREATE_POST,
  ADD_POST
} from "../constants/actionTypes";
import { apiPayloadCreator } from "../utils/appUtils";
import { call, takeEvery, select, put } from "redux-saga/effects";
import axios from "axios";
import { selectToken } from "../selectors/tokenSelectors";
import { generateSlug } from "../utils/postUtils";
import { push } from "connected-react-router";

const getPostsAC = createAction(API, apiPayloadCreator);

export const getPosts = () =>
  getPostsAC({
    url: "/posts/",
    method: "GET",
    onSuccess: setPosts,
    label: GET_POSTS
  });

/**
 * Redux-saga generator that watches for an action of type
 * REPORT_POST, and then runs the reportPost generator
 */
export function* watchReportPost() {
  yield takeEvery(REPORT_POST, reportPostSaga);
}

function* reportPostSaga(action) {
  try {
    const response = yield call(axios.post, "/email/", {
      subject: "Report Post",
      content: `A user has reported post: ${
        action.payload.title
      }, see more here: /posts/${action.payload.id}/${action.payload.slug}/`
    });
  } catch (error) {
    console.log(error);
  }
}

export function* watchCreatePost() {
  yield takeEvery(CREATE_POST, createPostSaga);
}

function* createPostSaga(action) {
  const token = yield select(selectToken);
  const headerParams = {
    Authorization: `JWT ${token}`
  };
  const apiCall = () => {
    let formData = new FormData();
    formData.append("title", action.payload.title);
    formData.append("content", action.payload.content);
    formData.append("thumbnail", action.payload.thumbnail);
    formData.append("tags", JSON.stringify(action.payload.tags));
    formData.append("slug", generateSlug(action.payload.title));
    return axios({
      method: "post",
      url: "/posts/",
      data: formData,
      headers: headerParams
    })
      .then(response => response.data)
      .catch(err => {
        throw err;
      });
  };

  try {
    const response = yield call(apiCall);
    yield put(push(`/posts/${response.id}/${response.slug}/`));
  } catch (error) {
    console.log(error);
  }
}

function setPosts(posts) {
  const postSchema = new schema.Entity("posts");
  const postListSchema = new schema.Array(postSchema);
  const normalizedData = normalize(posts, postListSchema);
  return {
    type: SET_POSTS,
    payload: normalizedData.entities.posts
  };
}

export const selectPost = createAction(SELECT_POST);
export const reportPost = createAction(REPORT_POST);
export const createPost = createAction(CREATE_POST);
