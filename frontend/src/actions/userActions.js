import { createAction } from "redux-actions";
import {
  IS_LOGGED_IN,
  SET_USER,
  VERIFY_LOGIN,
  LOGIN_USER,
  LOGOUT_USER,
  SIGNUP_USER
} from "../constants/actionTypes";
import { apiStart, apiEnd } from "../actions/apiActions";
import { takeEvery, put, call, select, all } from "redux-saga/effects";
import axios from "axios";
import { setToken } from "./tokenActions";
import { selectToken } from "../selectors/tokenSelectors";

/**
 * Redux-saga generator that watches for an action of type
 * VERIFY_LOGIN, and then runs the verifyLogin generator
 */
export function* watchVerifyLogin() {
  yield takeEvery(VERIFY_LOGIN, verifyLogin);
}

/**
 * Redux-saga generator that is called by watchVerifyLogin and queries the
 * api to verify that the current token in localStorage is still valid.
 * IF SO: SET loggedIn = true, and user = response.data.user
 * IF NOT: SET loggedIn = false, and user = {} (blank object}
 */
export function* verifyLogin() {
  try {
    apiStart(VERIFY_LOGIN);
    const token = yield select(selectToken);
    const response = yield call(axios.post, "/verify/", {
      // use redux-saga's select method to select the token from the state
      token: token
    });
    yield put(setUser(response.data.user));
    yield put(setLoggedIn(true));
    apiEnd(VERIFY_LOGIN);
  } catch (error) {
    apiEnd(VERIFY_LOGIN);
    yield put(setLoggedIn(false));
    yield put(setUser({})); // SET USER TO BLANK OBJECT
  }
}
/**
 * redux-actions action creator for logging the user out
 * @type {Object} Action creator like this: { type: LOGOUT_USER }
 */
export const logoutUserAC = createAction(LOGOUT_USER);

/**
 * redux-actions action creator for verifying login
 * @type {Object} Action creator like this: { type: VERIFY_LOGIN }
 */
export const verifyLoginAC = createAction(VERIFY_LOGIN);

/**
 * redux-actions action creator for logging the user in
 * @type {Object} Action creator like this: { type: LOGIN_USER }
 */
export const loginUserAC = createAction(LOGIN_USER);

export const signupUserAC = createAction(SIGNUP_USER);

export function* watchSignupUser() {
  yield takeEvery(SIGNUP_USER, signupUser);
}

function* signupUser({ payload }) {
  try {
    const response = yield call(axios.post, "/users/", payload);
    yield put(setToken(response.data.token));
    yield put(setUser(response.data.user));
  } catch (error) {
    console.log(error);
  }
}
/**
 * generator to  let redux-saga know it has to watch for LOGOUT_USER actions
 */
export function* watchLogoutUser() {
  yield takeEvery(LOGOUT_USER, logoutUser);
}

function* logoutUser() {
  yield put(setToken(""));
  yield put(setUser({}));
  yield put(setLoggedIn(false));
}

/**
 * generator to  let redux-saga know it has to watch for LOGIN_USER actions
 */
export function* watchLoginUser() {
  yield takeEvery(LOGIN_USER, loginUser);
}

/**
 * loginUser saga to log the user in, and set the state appropriately.
 * @param  {Object}    payload object containing the username and password supplied.
 */
function* loginUser({ payload }) {
  // LOGIN_USER actions will not be logged by redux-logger to prevent user and
  // pass from being in logs.
  try {
    const response = yield call(axios.post, "/token-auth/", payload);
    // run all three in parallel
    yield all([
      yield put(setToken(response.data.token)),
      yield put(setUser(response.data.user)),
      yield put(setLoggedIn(true))
    ]);
  } catch (error) {
    console.log(error);
    yield put(setLoggedIn(false));
    yield put(setUser({}));
  }
}

/**
 * Action creator for type SET_USER
 * @param {Object} user Object containing information about the user such as
 *   name, username, and email.
 * @return {Object} action to go to reducer and set user to payload
 */
const setUser = user => ({
  type: SET_USER,
  payload: user
});

/**
 * Action creator for type IS_LOGGED_IN
 * @param {Boolean} isLoggedIn true or false is the user logged in still.
 * @return {Object} action to go to reducer and set loggedIn to isLoggedIn
 */
const setLoggedIn = isLoggedIn => ({
  type: IS_LOGGED_IN,
  payload: isLoggedIn
});
