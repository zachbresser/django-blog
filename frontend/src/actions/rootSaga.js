import { call, all } from "redux-saga/effects";
import {
  watchVerifyLogin,
  watchSignupUser,
  watchLoginUser,
  watchLogoutUser
} from "./userActions";
import { watchReportPost, watchCreatePost } from "./postActions";

export default function* rootSaga() {
  yield all([
    call(watchVerifyLogin),
    call(watchLoginUser),
    call(watchLogoutUser),
    call(watchSignupUser),
    call(watchReportPost),
    call(watchCreatePost)
  ]);
}
