import { createAction } from "redux-actions";
import { GET_TOKEN, SET_TOKEN } from "../constants/actionTypes";

/**
 * redux-actions action creator for getting the token from the state
 * @type {Object} Action creator like this: { type: GET_TOKEN }
 */
export const getToken = createAction(GET_TOKEN);

/**
 * redux-actions action creator for setting the token to the state
 * @type {Object} Action creator like this: { type: SET_TOKEN }
 */
export const setToken = createAction(SET_TOKEN);
