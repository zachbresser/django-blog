import { handleActions } from "redux-actions";
import { SET_COMMENT_FORM_IS_ACTIVE } from "../constants/actionTypes";

export default handleActions(
  {
    [SET_COMMENT_FORM_IS_ACTIVE]: (state, action) => action.payload
  },
  false
);
