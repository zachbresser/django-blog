import { handleActions } from "redux-actions";
import { GET_POSTS, SET_POSTS, ADD_POST } from "../constants/actionTypes";

export default handleActions(
  {
    [GET_POSTS]: (state, action) => state,
    [SET_POSTS]: (state, action) => action.payload,
    [ADD_POST]: (state, action) => {
      let post = action.payload;
      let statePosts = state;
      let newPosts = {
        ...statePosts,
        [post.id]: post
      };
      return newPosts;
    }
  },
  {}
);
