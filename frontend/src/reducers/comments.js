import { handleActions } from "redux-actions";
import {
  GET_COMMENTS,
  SET_COMMENTS,
  ADD_COMMENT
} from "../constants/actionTypes";

export default handleActions(
  {
    [GET_COMMENTS]: (state, action) => state,
    [ADD_COMMENT]: (state, action) => {
      const comments = {
        ...state.comments,
        ...action.payload
      };
      return {
        ...state,
        ...comments
      };
    },
    [SET_COMMENTS]: (state, action) =>
      Boolean(action.payload) ? action.payload : state
  },
  {}
);
