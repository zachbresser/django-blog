import { handleActions } from "redux-actions";
import produce from "immer";
import { GET_POSTS } from "../constants/labels";
import { VERIFY_LOGIN } from "../constants/actionTypes";
import { API_START, API_END } from "../constants/actionTypes";

export default handleActions(
  {
    [API_START]: produce((state, action) => {
      if (action.payload === GET_POSTS) state[GET_POSTS] = true;
      if (action.payload === VERIFY_LOGIN) state[VERIFY_LOGIN] = true;
    }),
    [API_END]: produce((state, action) => {
      if (action.payload === GET_POSTS) state[GET_POSTS] = false;
      if (action.payload === VERIFY_LOGIN) state[VERIFY_LOGIN] = false;
    })
  },
  {}
);
