import { combineReducers } from "redux";
import comments from "./comments";
import posts from "./posts";
import isLoading from "./isLoading";
import selectedPost from "./selectedPost";
import commentFormIsActive from "./commentFormIsActive";
import loggedIn from "./loggedIn";
import user from "./user";
import token from "./token";
import { connectRouter } from "connected-react-router";

const rootReducer = history => {
  console.log("history: ", history);
  return combineReducers({
    router: connectRouter(history),
    comments,
    isLoading,
    loggedIn,
    selectedPost,
    user,
    token,
    commentFormIsActive,
    posts
  });
};

export default rootReducer;
