import { handleActions } from "redux-actions";
import { SET_USER } from "../constants/actionTypes";

export default handleActions(
  {
    [SET_USER]: (state, action) => action.payload
  },
  {}
);
