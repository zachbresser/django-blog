import { handleActions } from "redux-actions";
import { IS_LOGGED_IN } from "../constants/actionTypes";

export default handleActions(
  {
    [IS_LOGGED_IN]: (state, action) => action.payload
  },
  localStorage.getItem("token") ? true : false
);
