import { handleActions } from "redux-actions";
import { SELECT_POST } from "../constants/actionTypes";

export default handleActions(
  {
    [SELECT_POST]: (state, action) => action.payload
  },
  1
);
