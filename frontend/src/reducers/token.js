import { handleActions } from "redux-actions";
import { GET_TOKEN, SET_TOKEN } from "../constants/actionTypes";

export default handleActions(
  {
    [GET_TOKEN]: (state, action) => state,
    [SET_TOKEN]: (state, action) => action.payload
  },
  ""
);
