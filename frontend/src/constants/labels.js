export const GET_POSTS = "GET_POSTS";
export const GET_COMMENTS = "GET_COMMENTS";
export const GET_POST = "GET_POST";
export const IS_AUTHOR_STAFF = "IS_AUTHOR_STAFF";
export const COMMENT_FORM_IS_ACTIVE = "commentFormIsActive";
