/**
 * if user is accessing via localhost, [::1], or 127.x.x.x
 * taken from `create-react-app`'s registerServiceWorker.js
 * @type {Boolean}
 */
const isLocalHost = Boolean(
  window.location.hostname === "localhost" ||
    // [::1] is the IPv6 localhost address.
    window.location.hostname === "[::1]" ||
    // 127.0.0.1/8 is considered localhost for IPv4.
    window.location.hostname.match(
      /^127(?:\.(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}$/
    )
);

/**
 * Object containing
 * @type {Object}
 */
const APIConfig = {
  SCHEME: "http://",
  URL: isLocalHost
    ? "127.0.0.1/api/v1/"
    : window.location.hostname.toString() + "/api/v1/",
  COMMENTS: "comments/",
  POSTVIEW: "posts/view/",
  POSTDATE: "posts/",
  USERS: "users/",
  CURRENT_USER: "current_user/",
  TOKEN_AUTH: "token-auth/"
};

export default APIConfig;
