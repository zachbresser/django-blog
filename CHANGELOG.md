<!-- This is for all changes to the application. Newest changes towards the top -->
<!-- Versioning is like so: -->
<!-- X.X.X?-(alpha|beta)? [Major].[Minor].[BugFixes](optional)-[DevStatus](optional)- -->
<!-- PRs will not be accepted without the following: -->
<!--     * Pull Request  -->
<!--     * Issue (if applicable such as bug fixes)   -->
<!--     * Documentation (see README.md for details) -->

### Version 0.1-alpha:

* You can now view comments using the API middleware
* You can now add comments using the API middleware
* You can now view posts using the API middleware
* React-redux, redux, redux-logger, and redux-persist are all in process of being implemented. 


