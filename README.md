# TechHorizon

This is a blog application that utilizes the following technologies.

**Note: TechHorizon is MIT licensed and always will be as long as I am developing it**

## 🖥️ Backend:

---

- **django 2.1**

- **Django Rest Framework**

## 🎨 Frontend:

---

- **create-react-app** (Facebook, uses WebPack I believe)

- **React**

Prior to this project I had experience with **Django** but no experience with **React** or **Django Rest Framework**.

## Deployment:

---

**Backend:**

Use any number of solutions including gunicorn, mod-wsgi, etc. See [Django's Deployment Guide](https://docs.djangoproject.com/en/2.1/howto/deployment/) for more information.

**Frontend:**

Use `npm run build` to generate static files, serve them from any webserver (S3 Static hosting, NGinx, Apache, etc)

## Contribute to TechHorizon:

All Issues or Merge Requests should use the template to ensure the proper information is given.

All commits should follow the following scheme: (?): <COMMIT_MESSAGE> where ? is enhancement, feature, bug, or an issue number. If you do not have the (?): part Gitlab will reject it off of the following regex: `\((enhancement|enhance|bug|feature|feat|#\d+)\):( )?.*/gmi`

### Documentation

---

#### React

---

django-blog/TechHorizon uses React-doc-generator for generating documentation based off the code. In order for it to function properly you must have a few things:

- Top level comment above React.Component like so:

```
/**
 * This is an react component that echos a heading stored in props.
   [You must have a comment like this]
 */
class MyFirstReactComponent extends Component {
  render() {
    return <div>{this.props.heading} </div>;
  }
}
```

- PropTypes definition like so:

```
MyFirstReactComponent.propTypes = {
  /**
   * The heading to echo
   */
  heading: PropTypes.string,
};
```

Optionally default values for props can be included in the documentation like so:

```
MyFirstReactComponent.defaultProps = {
  heading: 'Tech Horizon',
};
```

## ☑️ ToDo:

---

### Stuff that must be done before the site can go live

- ~~once basic design is done, integrate `react-router`~~

- implement signup

- implement ability to ~~create, read~~, update, and ~~delete~~ a comment

- implement ability to create,~~read~~, update, and delete a post

- redesign existing pages (and all new pages) to use material-ui and remove react-bootstrap as a dependency

- user profiles that all posts/comments from a user

* only show new comment box when user is logged in (EASY PICKINGS)

* integrate a state management system such as `redux` or `mobx`

* integrate anymail for async email backends

### Stuff that can be done after the site goes live

- determine performance, optimize API calls if needed (on backend and frontend)

- allow guests to share, favorite, and follow posts/authors (such as alerts when an author posts a new article)
